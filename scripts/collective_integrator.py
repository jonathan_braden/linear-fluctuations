#!/usr/bin/python

from scipy.integrate import odeint, quad
from scipy.special import ellipj
import numpy as np

def potential(x):
    f=-8./3. + 8.*x + 12./np.tanh(2.*x) - (24.*x+8.)/np.tanh(2.*x)**2 + 16.*x/np.tanh(2.*x)**3
    return 2.**0.5*f

def vprime(x):
    csh = 1./(np.sinh(2.*x))
    cot = 1./(np.tanh(2.*x))
    f = 8. - 24.*csh**2 + 4.*(24.*x+8.)*csh**2*cot - 24.*cot**2 + 16.*cot**3 - 96.*x*cot**2*csh**2
    return 2.**0.5*f

def derivs(y,t):
#    gamma = (1.-y[1]**2)**(-0.5)
    vp = vprime(y[0])
    return [ y[1], -vp ]

# Get period of system
#t_nokin=2.**0.5*quad() 
#t_wkin=2.**0.5*quad()

#xinit=[ 10., 0. ]
#f=odeint(derivs, , t)

xvals=np.linspace(-0.5,2.,1000)
vp=vprime(xvals)
pot=potential(xvals)

rmax=2.
xinit=[rmax, 0.]
tvals=np.linspace(0.,500.,1000)
f=odeint(derivs,xinit,tvals)
rcomp=rmax*np.ones(len(tvals))

import matplotlib.pyplot as plt
plt.plot(tvals,f[:,0])
plt.plot(tvals,rcomp)
