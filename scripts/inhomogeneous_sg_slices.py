#!/usr/bin/python

#
# Script to plot floquet slices for SG breather
# Currently, I assume I pump out the Floquet exponent in time units,
# and here I convert back to per period (which is invariant under time-redeft
#

file_name="/media/data/Documents_tmp/Papers/bubble_papers/linear_fluc/floquet_sg.dat"

import sys
sys.path.insert(0, '/home/jbraden/Documents/python_scripts/')

import myfileutils as myf
import matplotlib.pyplot as plt
import numpy as np

# Read in the various floquet exponents
# In what follows I assume first column is phi_max, second is k^2, thirds is exponent
#a=np.genfromtxt(file_name,usecols=[0,1,2])
#vparam=np.reshape(a[:,0],
#kvals=np.reshape(a[:,1],
#floqvals=np.reshape(a[:,2],
#period = 2.*np.pi*(1.+vparam**2)**0.5/vparam

vals=myf.read_timeblock(file_name,[0,1,2])

mycmap=plt.cm.jet
for i in range(len(vals)):
    vparam = vals[i][0][0]
    period = 2.*np.pi * (1+vparam**2)**0.5/vparam
    kvals=vals[i][1]
    floqvals=vals[i][2]
# convert the v value to a phi_max value
    phimax = 4.*np.arctan(1/vals[i][0][0])
    plt.plot(kvals,period*np.array(floqvals), 'r-', linewidth=2.0, label=r'$\phi_{max}=%.2f$'%phimax, color=mycmap(i))

plt.xlabel(r'$k_\perp^2$', fontsize=30)
plt.ylabel(r'Floquet Exponent ($\mu$)',fontsize=30)
plt.legend(bbox_to_anchor=(0,0,1.,0.95))
