#!/usr/bin/python
# Filename: pot_plot.py

#
# Make the plot of the potential that is being used in the paper
#

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

golden_ratio=0.5*(np.sqrt(5.)-1.)
fig_width=8.5
fig_height=golden_ratio*fig_width

mpl.rc('xtick',labelsize=20)
mpl.rc('ytick',labelsize=20)

# Set some plot properties
# font size
fsize = 30

#
# Define the potential parameters in here
#
vvals=(0.01, 1., 1./(2.**0.5-1.))
print len(vvals)
vlabels=(r'$0.01$',r'$1$',r'$1/(\sqrt{2}-1)$')

def breather(x, t, v):
    gamma = 1./(1+v**2)**0.5
    return 4.*np.arctan(np.cos(gamma*v*t) / (v*np.cosh(gamma*x)))

def eff_mass(x,t,v):
    return np.cos(breather(x,t,v))

#
# Now make the plot
#
plt.xlabel(r'$x$', fontsize=fsize)
plt.ylabel(r'$\phi$', fontsize=fsize)

xvals = np.linspace(-10.,10.,101)
for i in range(len(vvals)):
    yvals=breather(xvals, 0., vvals[i])
    plt.plot(xvals, yvals, linewidth=2.0, label=r'$v=$'+vlabels[i])

plt.subplots_adjust(bottom=0.15,left=0.15)
#plt.legend(frameon=False)
plt.legend(bbox_to_anchor=(0,0,1.15,1.15),fontsize=24)

fig=plt.gcf()
fig.set_size_inches(fig_width,fig_height)
fig.savefig('breathers.pdf', bbox_inches=0)
plt.show()

plt.xlabel(r'$x$', fontsize=fsize)
plt.ylabel(r'$\partial_{\phi\phi}V(\phi_{breather})$', fontsize=fsize)

xvals = np.linspace(-10., 10., 101)
for i in range(len(vvals)):
    yvals=eff_mass(xvals, 0., vvals[i])
    plt.plot(xvals, yvals, linewidth=2.0, label=r'$v=$'+vlabels[i])

plt.subplots_adjust(bottom=0.15)
plt.subplots_adjust(left=0.17)
plt.legend(bbox_to_anchor=(0.,0.,1.15,1.),fontsize=24,loc='lower right')

fig=plt.gcf()
fig.set_size_inches(fig_width,fig_height)
fig.savefig('effmass_breathers.pdf', bbox_inches=0)
plt.show()
