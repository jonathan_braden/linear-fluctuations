#!/usr/bin/python
# Filename: pot_plot.py

#
# Make the plot of the potential that is being used in the paper
#

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)
# Set some plot properties
# font size
fsize = 26

#
# Define the potential parameters in here
#
delvals=(0., 1./30., 1./10., 1./5.)
print len(delvals)
dellabels=(r'$0$',r'$\frac{1}{30}$',r'$\frac{1}{10}$',r'$\frac{1}{5}$',r'$\frac{1}{3}$',r'$\frac{2}{3^{3/2}}$')

# Define the potential
def pot(x, d):
    return 0.25*(x**2-1)**2 - d*(x-1.)

#
# Now make the plot
#
plt.ylabel(r'$V(\phi)/\lambda \phi_0^4$', fontsize=fsize)
plt.xlabel(r'$\phi/\phi_0$', fontsize=fsize)

xvals = np.linspace(-1.5,1.5,101)
for i in range(len(delvals)):
    yvals=pot(xvals, delvals[i])
    plt.plot(xvals, yvals, linewidth=2.0, label=r'$\delta=$'+dellabels[i])

plt.subplots_adjust(bottom=0.15)
plt.subplots_adjust(left=0.15)
#plt.legend(frameon=False)
plt.legend(bbox_to_anchor=(0,0,0.95,1.),fontsize=24)

plt.savefig('potential.pdf', bbox_inches=0)
