#!/usr/bin/python

#
# Open all of the data files that contain the various eigenfunctions
#
#

import sys
sys.path.insert(0,'/home/jbraden/Documents/python_scripts/')

import myfileutils as myf
import matplotlib.pyplot as plt
import numpy as np
import glob

basedir="/media/data/Code/SpatialFloquet/mode_functions/internal_mode/"

files=glob.glob(basedir+"efunc*.dat")

# Put in desired kvalues by hand here
kval=[0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45]
efunc=[]
# extract the eigenfunctions and corresponding momenta, normalize to the midpoint is positive
for k in kval:
    f=basedir+'efunc_k2_'+str(k)+'_a1.dat'
    print f
    data=myf.read_timeblock(f,[0,2,3])
    xmid=len(data[0][0])/2
    efunc.append( [ np.array(data[0][0])-data[0][0][xmid], np.sign(data[0][1][xmid])*np.array(data[0][1]), np.sign(data[0][1][xmid])*np.array(data[0][2])])

#kval=[]
#efunc=[]
#for f in files:
#    kval.append(f.lstrip(basedir+'efunc_k').rstrip('.dat'))
#    data=myf.read_timeblock(f,[0,2])
# Make sure the middle point is positive
#    xmid=len(data[0][0])/2
#    efunc.append( [ np.array(data[0][0])-data[0][0][xmid], np.sign(data[0][1][xmid])*np.array(data[0][1])])
    

# Here is the actual figure making
# Ok, now make a bunch of vertical plots, with no space in between

from pylab import *
f=figure()
subplots_adjust(hspace=0.001)
nplots=len(efunc)

lwid=2.0  # linewidth
xmin=-20
xmax=20
ymin=-0.3
ymax=0.3
phicol='b-'
phidotcol='r--'
rbound=0.85

ax=nplots*[0]
# To do: make sure length is greater than 1
curplot=nplots*100+10+1
ax[0]=subplot(curplot)
ax[0].plot(efunc[0][0],efunc[0][1], phicol, linewidth=lwid)
ax[0].plot(efunc[0][0],efunc[0][2], phidotcol, linewidth=lwid)
yticklabels=ax[0].get_yticklabels()
setp(yticklabels, visible=False)
xticklabels=ax[0].get_xticklabels()
setp(xticklabels, visible=False)
ylabel(r'$k_\perp^2 = '+str(kval[0])+'$',fontsize=14, rotation="horizontal",  horizontalalignment='right', verticalalignment="center")
xlim(xmin,xmax)
ylim(ymin,ymax)
#legend(title=r'$k_2^2 = '+str(kval[0])+'$')

for i in range(1,len(efunc)):
    curplot=nplots*100+10+i+1
    ax[i] = subplot(curplot, sharex=ax[0])
    ax[i].plot(efunc[i][0],efunc[i][1], phicol, linewidth=lwid)
    ax[i].plot(efunc[i][0],efunc[i][2], phidotcol, linewidth=lwid)
    yticklabels=ax[i].get_yticklabels()
    setp(yticklabels, visible=False)
    xticklabels=ax[i].get_xticklabels()
    setp(xticklabels, visible=False)
    ylabel(r'$k_\perp^2 = '+str(kval[i])+'$', fontsize=14, rotation="horizontal", horizontalalignment='right')
    xlim(xmin,xmax)
    ylim(ymin,ymax)

# Turn last set of axis ticks back on
xticklabels=ax[8].get_xticklabels()
setp(xticklabels,visible=True)

# Now add the global legend
subplots_adjust(bottom=0.15)
subplots_adjust(left=0.15)
subplots_adjust(right=0.9)
subplots_adjust(top=0.9)
legend( (r'$\phi$',r'$\dot{\phi}$'), bbox_to_anchor=(0.7,1.), loc='lower left', fancybox=True, shadow=True)
xlabel(r'$x\sqrt{1-\epsilon^2}$', fontsize=16)
suptitle(r'$\epsilon = 0.2$',verticalalignment="top", fontsize=16)
