import numpy as np

def S_2(x):
    f=4.*(2.*x/np.tanh(2*x)-1.) #/np.sinh(2*x)**2
    return f

def S_3(x):
    f=2.*(4.*x*(2.+np.cosh(4.*x))-3.*np.sinh(4.*x))/np.sinh(2.*x)**2
    return f

def S_4(x):
    ch=1./np.tanh(2.*x)
    f=x*(12.-20./np.tanh(2.*x)**2)/np.tanh(2.*x) - 8./3. + 10.*ch**2
    return f

# Potentials set to vanish at infinity
def V_pot(x):
    f=-4./3. -8.*x + (12.-4.*x)/np.tanh(2.*x) - (10.+8.*x)/np.tanh(2.*x)**2 + 20.*x/np.tanh(2.*x)**3 - 16.*x/np.sinh(2.*x)**2
    return f

def V_eff(x):
    f=-4. + 8.*x + 12./np.tanh(2.*x) - (24.*x+8.)/np.tanh(2.*x)**2 + 16.*x/np.tanh(2.*x)**3
    return 2.**0.5*f

def V_asymptotic(x):
    f = -8.*2.**0.5*np.exp(-4.*x)
    return f

def vprime(x):
    cot=1./np.tanh(2.*x)
    csh=1./np.sinh(2.*x)
    f = 8. - 24.*csh**2 + 4.*(24.*x+8.)*csh**2*cot - 24.*cot**2 + 16.*cot**3 - 96.*x*cot**2*csh**2
    return 2.**0.5*f

def vprime_new(x):
    cot = 1./np.tanh(2.*x)
    f = 32. - (96.*x+32.)*cot + (96.*x-48)*cot**2 + (96.*x + 48.)*cot**3 - 96.*x*cot**4
    return 2.**0.5*f

def vprime_taylor(x):
    f = 64.*x/5. - 128.*x**2/5. + 512.*x**3/105. + 512.*x**4/21. - 2048.*x**5/175. #- 4096.*x**6/225. + 212992.*x**7/17325. + 8192.*x**8/693.
    return f

import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=20)
mpl.rc('ytick',labelsize=20)

#plt.plot(xvals,S_2(xvals),label="s2")
#plt.plot(xvals,S_3(xvals),label="s3")
#plt.plot(xvals,S_4(xvals),label="s4")
#plt.legend()
#plt.show()

xvals=np.linspace(-0.5,2.,1000)
potential=2.**0.5*(S_2(xvals)-S_3(xvals)-S_4(xvals)) -2.**1.5/3.
grad=(4./3.-S_2(xvals)/np.sinh(2.*xvals)**2)/2.**0.5 - 2.**1.5/3.
veff=potential+grad
veff2=V_eff(xvals)
msq=2.*2.*2.**0.5/3.*np.ones(len(xvals))
kinetic=msq*(1.+S_2(xvals)/np.sinh(2.*xvals)**2*3./4.)

plt.plot(xvals,veff,label=r"$V_{\mathrm{eff}}(r)$",linewidth=2)
#Uncomment this when I get the factor correct
plt.plot(xvals,S_2(xvals)/np.sinh(2.*xvals)**2*3./4.,label=r"$K(r)$",linewidth=2)
#plt.plot(xvals,kinetic)
plt.plot(xvals,potential,label=r"$V_{\mathrm{pot}}(r)$",linewidth=2)
plt.plot(xvals,grad,label=r"$V_{\mathrm{grad}}(r)$",linewidth=2)
plt.plot(xvals,V_asymptotic(xvals),label=r"$-8\sqrt{2}e^{-2\sqrt{2}r}$",linewidth=2)
#plt.plot(xvals,msq,'k')
plt.legend(loc='lower right',fontsize=20)
plt.xlim(-0.5,2)
plt.ylim(-3.,1.5)
plt.xlabel(r'$\omega r$',fontsize=30)
plt.ylabel(r'$V_{\mathrm{eff}}$',fontsize=30)
plt.tight_layout()
plt.savefig('collective_potential.pdf')
plt.show()

#plt.plot(xvals,(4.*2.**0.5/3.-veff)/kinetic)
#plt.plot(xvals,(4.*2.**0.5/3.-veff))

dx=xvals[1]-xvals[0]
xnew=np.linspace(-1.e-5,1.e-5,500)
vp=vprime(xnew)
vp2=vprime_taylor(xnew)
plt.plot(xnew,vp)
plt.plot(xnew,vp2)
plt.show()
