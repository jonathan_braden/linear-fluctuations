#!/usr/bin/python

#
# Specify the data file to use
#
#file_name="/home/jbraden/Documents/Papers/domain_wall/data/floquet_charts/internal_dx0.25_L64/floquet_chart.dat"
file_name="/media/data/Code/SpatialFloquet_gauss/floquet_internal_L72w_n256.dat"

#
# Start by adding analysis routine folder
#
import sys
sys.path.insert(0, '/home/jbraden/Documents/python_scripts/')

import numpy as np
import myfileutils as myf
import matplotlib.pyplot as plt
import matplotlib.cm as cm

# Read in the v values, kvalues and floquet exponents
vals=myf.read_timeblock(file_name,[0,1,2])

# Plot individual slices of floquet vs. kvalue
#for i in range(len(vals)):
#    kvals=vals[i][1]
#    floqvals=vals[i][2]
#    plt.plot(kvals,floqvals)

#plt.ylabel(r'Lyapanov Exponent ($\mu_{max}$)')
#plt.xlabel(r'a')
#plt.show()

ampvals=[]
kvals=[]
floqvals=[]
# Get data for the contour plot
for i in range(len(vals)):
    ampvals.append(vals[i][0])
    kvals.append(vals[i][1])
    floqvals.append(vals[i][2])

ampvals=np.array(ampvals)
kvals=np.array(kvals)
floqvals=np.array(floqvals)
# Make the Contour Plot
mycmap=cm.OrRd
# Adjust the contour levels here as needed
plt.contourf(ampvals,kvals,floqvals,15,cmap=mycmap)
plt.ylim((0,0.8))
cb=plt.colorbar(orientation='vertical')
cb.ax.set_ylabel(r'Lyapanov Exponent $(\mu_{max}T)$')
plt.ylabel(r'$k_{\perp}^2$')
plt.xlabel(r'$A_{shape}$')

plt.show()
