import numpy as np
import matplotlib as mpl

golden_ratio=0.5*(np.sqrt(5.)-1.)
fig_width=8.5
fig_height=golden_ratio*fig_width

mpl.rc('xtick',labelsize=20)
mpl.rc('ytick',labelsize=20)
fsize=30

#vval=1.
#vtext=r'$1$'
#vstr='1'
#ticks_effmass=[-1.,-0.5,0,0.5,1]
#add_kink=False

vval=1./(2.**0.5-1.)
vtext=r'$(\sqrt{2}-1)^{-1}$'
vstr='sqrt2'
ticks_effmass=[0.,0.2,0.4,0.6,0.8,1.]
add_kink=False

#vval=0.01
#vtext=r'$0.01$'
#vstr='0.01'
#ticks_effmass=[-1,-0.5,0.,0.5,1.]
#add_kink=True

def m2eff(x,t,v):
    gamma = 1./(1.+v**2)**0.5
    f=np.cos(t)/v/np.cosh(gamma*x)
    f=np.cos(4.*np.arctan(f))
    return f

def breather(x,t,v):
    gamma = 1./(1.+v**2)**0.5
    f=4.*np.arctan(np.cos(t)/v/np.cosh(gamma*x))
    return f

def xkink(t,v):
    gamma = 1./(1.+v**2)**0.5
    f=-np.sign(np.cos(t))*np.arccosh(np.abs(np.cos(t))/v) / gamma
    return f

xvals=np.linspace(-10.,10.,101)
tvals=np.linspace(0,4.*np.pi,101)
mvals=[]
fvals=[]
for i in range(len(tvals)):
    mvals.append(m2eff(xvals,tvals[i],vval))
    fvals.append(breather(xvals,tvals[i],vval))

import matplotlib.pyplot as plt
plt.contourf(xvals,tvals,mvals,15,cmap=plt.cm.PuBu_r)
plt.xlim(-10,10)
plt.ylim(0,4.*np.pi)
plt.xlabel(r'$x$',fontsize=fsize)
plt.ylabel(r'$\gamma v t$',fontsize=fsize)
cbar=plt.colorbar(ticks=ticks_effmass)
cbar.ax.set_ylabel(r'$V_{\phi\phi}(\phi_{\mathrm{breather}}(x,t))$',fontsize=fsize)

plt.text(-9.5,3.5*np.pi,r'$v=$'+vtext,fontsize=fsize, bbox=dict(facecolor='white',alpha=1))
plt.subplots_adjust(bottom=0.15,left=0.15)

if add_kink:
    xloc = xkink(tvals,vval)
    plt.plot(xloc,tvals,'r',linewidth=1.5)

fig=plt.gcf()
fig.set_size_inches(fig_width,fig_height)
fig.savefig('effmass_sg_v'+vstr+'.pdf')
plt.show()

cvals=np.linspace(-np.pi,np.pi,29)
plt.contourf(xvals,tvals,fvals,cvals,cmap=plt.cm.RdYlBu_r,extend='both')
plt.xlim(-10,10)
plt.ylim(0,4.*np.pi)
plt.xlabel(r'$x$',fontsize=fsize)
plt.ylabel(r'$\gamma v t$',fontsize=fsize)
cbar=plt.colorbar(ticks=[-np.pi,-0.5*np.pi,0,0.5*np.pi,np.pi],format=r"%2.1f")
cbar.ax.set_ylabel(r'$\phi_{\mathrm{breather}}$',fontsize=fsize)
cbar.ax.set_yticklabels([r'$-\pi$',r'$-\frac{\pi}{2}$',r'$0$',r'$\frac{\pi}{2}$',r'$\pi$'])
cbar.ax.get_yticklabels()[0].set_fontsize(24)
cbar.ax.get_yticklabels()[1].set_fontsize(28)
cbar.ax.get_yticklabels()[2].set_fontsize(24)
cbar.ax.get_yticklabels()[3].set_fontsize(28)
cbar.ax.get_yticklabels()[4].set_fontsize(24)

plt.text(-9.5,3.5*np.pi,r'$v=$'+vtext,fontsize=fsize,bbox=dict(facecolor='white',alpha=1))
plt.subplots_adjust(bottom=0.15,left=0.15)

fig=plt.gcf()
fig.set_size_inches(fig_width,fig_height)
fig.savefig('breather_v'+vstr+'.pdf')
plt.show()
