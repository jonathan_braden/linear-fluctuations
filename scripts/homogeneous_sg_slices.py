#1/usr/bin/python

file_name="/home/jbraden/Documents/exponents/sg_exp.dat"

import sys
sys.path.insert(0, '/home/jbraden/Documents/python_scripts/')

import myfileutils as myf
import matplotlib.pyplot as plt

# Read in the various floquet exponents
# In what follows I assume first column is phi_max, second is k^2, thirds is exponent
vals=myf.read_timeblock(file_name,[0,1,2])

mycmap=plt.cm.jet
for i in range(len(vals)):
    kvals=vals[i][1]
    floqvals=vals[i][2]
    plt.plot(kvals,floqvals, 'r-', linewidth=2.0, label=r'$\phi_{max}='+str(vals[i][0][0])+'$', color=mycmap(i))

plt.xlabel(r'$k^2$', fontsize=30)
plt.ylabel(r'Floquet Exponent ($\mu$)',fontsize=30)
plt.legend(bbox_to_anchor=(0,0,1.,0.95),fontsize=20)
