\section{Numerical Approach and Convergence Tests}
\label{sec:numerics}
In this appendix we briefly summarize the numerical techniques used in this paper.
First we describe our strategy for numerical time-evolution.
We used different approaches when solving for the nonlinear background~\eqref{eqn:wall_background} and the linear fluctuations~\eqref{eqn:wall_linearfluc}.
For the one-dimensional nonlinear wave equation~\eqref{eqn:wall_background}, we used a 10th order accurate Gauss-Legendre quadrature based method.
Explicitly, this is a specific choice of implicit Runge-Kutta process.
Given an initial condition ${\bf y}_t$  to $d{\bf y}/dt = {\bf H}({\bf y})$ at time $t$, the solution at time $t+dt$ is obtained by solving
\begin{align}
  {\bf f}^{(i)} &= {\bf H}\left({\bf y}_t + dt\sum_{j=1}^\nu a_{ij}{\bf f}^{(j)}\right) \\
  {\bf y}_{t+dt} &= {\bf y}_t + dt\sum_{i=1}^\nu b_i{\bf f}^{(i)}
\end{align}
where $a_{ij}$ and $b_{i}$ are numerical constants defining the process.
For Gauss-Legendre methods, $a_{ij}$ and $b_{i}$'s are solutions to
\begin{align}
  \label{eqn:gl_coeffs}
  \sum_{j=1}^\nu a_{ij}c_j^{l-1} &= \frac{c_i^l}{l} \qquad l=1,\dots,\nu \\
  \sum_{j=1}^\nu b_jc_j^{l-1} &= l^{-1} \, .
\end{align}
The $c_i$'s are the roots of $P_\nu(2c-1)$ where $P_\nu(x)$ is the Legendre polynomial of degree $\nu$.
These relations arise by approximating the time-evolution integrals using Gauss-Legendre quadrature.
Because of the excellent convergence properties of quadrature approximations, the result is an order $2\nu$ integrator.\footnote{Here we take the order of the integrator (denoted by n) to be the highest power in $dt$ for which the approximate solution is exact.  This means the leading order error term is $\sim dt^{n+1}$.}
If the reader would like explicit formulae for $\nu$ up to 5 please see Table 2 of Butcher~\cite{Butcher:1964}, although it is far easier in practice to simply solve~\eqref{eqn:gl_coeffs} numerically.

As for the linear fluctuation equation~\eqref{eqn:wall_linearfluc}, we instead employed Yoshida's~\cite{Yoshida:1990} operator-splitting technique that was introduced into the preheating community by Frolov and Huang.  For further details see for example~\cite{Huang:2011gf,Sainio:2012mw,Yoshida:1990}.  For this set of integrators, the solution to $df/dt = H(f)$ is first written as $f(t+dt) = e^{{\bf H}dt}f(t)$, where ${\bf H}$ should now be interpreted as an operator acting on $f$.
We decompose ${\bf H}=\sum_i{\bf H_{i}}$ so the action of each individual ${\bf H_i}$ on $f$ is simple to compute accurately.
Finally, we re-express the time evolution operator $U\equiv e^{{\bf H}dt}$ as a product of exponentials for the individual ${\bf H}_i$ operators, $e^{{\bf H}dt} = U(w_M)U(w_{M-1})\dots U(w_0)U(w_1)\dots U(w_M) + \mathcal{O}(dt^{n+1})$, where $U(w_i) \equiv e^{w_i{\bf H_1} dt/2}e^{w_i{\bf H_2} dt}e^{w_i{\bf H_1}dt/2}$ is a second-order accurate time-evolution operator for time-step $w_idt$ and we have specialized to the case of an operator split into only two parts for simplicity.
Via clever choices of the number and value of the numerical coefficients $w_i$, integrators of various orders $n$ may be constructed.
For this paper, we use an $\mathcal{O}(dt^6)$ method with coefficients $w_i$ given by
\begin{align}
  \notag w_1 &= -1.17767998417887100694641568096431573 \\
  \notag w_2 &= 0.235573213359358133684793182978534602 \\
  \notag w_3 &= 0.784513610477557263819497633866349876 \\
  w_0 &= 1 - 2(w_1+w_2+w_3) = 1.31518632068391121888424972823886251 \, .
\end{align}
%~\cite{McGlaughlan}

Both of the Gauss-Legendre and Yoshida approaches are symplectic integrators for Hamiltonian systems.
Therefore, we found it convenient to use Hamilton's form for the evolution equations.
With the exception of the collective coordinate location for the bouncing walls in the double well, 
all of the Hamiltonians used in this paper can be split into two exactly solvable pieces so that $\mathcal{H}=\mathcal{H}_1+\mathcal{H}_2$.
For reference, we provide these splits below, even though they are not required for the Gauss-Legendre method used to solve the nonlinear background wave equations.
For the planar walls the the split terms are (up to an overall normalization)
\begin{equation}
  \mathcal{H}_{planar,1} = \sum_i \frac{\pi_{\phi,i}^2}{2} \, , \qquad \mathcal{H}_{planar,2} = \sum_i\frac{G[\phi_i]}{2dx^2} + V(\phi_i) \, .
  \label{eqn:planar_hamiltonian}
\end{equation}
Meanwhile, the linearized fluctuations evolve in the Hamiltonian
\begin{equation}
  \mathcal{H}_{fluc,1} = \sum_i \frac{\pi_{\delta\phi,i}^2}{2} \, , \qquad \mathcal{H}_{fluc,2} = \sum_i \frac{G[\delta\phi_i]}{2dx^2} + \frac{1}{2}V''(\phi_{bg}(x,t))\delta\phi_i^2 \, .
  \label{eqn:fluc_hamiltonian}
\end{equation}
Finally, the Hamiltonian for the $SO(2,1)$ invariant bubbles is
\begin{equation}
  \mathcal{H}_{bubbles,1} = \sum_i \frac{\pi_{\phi,i}^2}{2s^2} \, , \qquad \mathcal{H}_{bubbles,2} = \sum_i s^2\left( \frac{G[\phi_i]}{2} + V(\phi_i) \right) \, .
  \label{eqn:bubble_hamiltonian}
\end{equation}
In all three cases, $\pi_{f,i}$ represents the canonical momentum for field $f$ at lattice site $i$.
The operator $G[\phi_i]$ is a discrete approximation to $\left(\partial_x\phi(x_i)\right)^2$.

Finally we describe the spatial discretization of the system.
For all production runs we used a Fourier pseudospectral approximation for the field derivatives.
The only derivative appearing in the various equations of motion is the one-dimensional \laplacian\ along the collision axis $\partial_{xx}$.
Therefore, in practice the system was evolved in real space, with the \laplacian\ evaluated in Fourier space through the use of the FFT.
Although the resulting FFT and inverse FFT are numerically more expensive than a finite-difference approximation,
the continuum limit is approached much more rapidly as seen in~\figref{fig:floquet_convergence}.
This is especially important when computing Floquet exponents, as our approach requires solving $2N$ individual PDE's in order to form the fundamental matrix where $N$ is the number of grid points.
As well, in order to maintain a fixed accuracy in the time-integration, the ratio $dx/dt$ should be kept constant meaning that the total work required scales as $N^3$ for a finite-difference approximation and $N^3\log(N)$ for a pseudospectral approach.\footnote{When comparing run times, the reader should keep in mind that the limiting factor on modern computing architecture is often the speed at which data can be obtained from memory, not necessarily the number of floating point operations.  However, for the problems we were concerned with the spectral approach proved to be much faster if an accuracy better than the tenth of a percent level was desired.}
Since the pseudospectral calculations converged using much fewer lattice sites than the finite-difference stencils, the pseudospectral approach ended up requiring less CPU time while simultaneously being (orders of magnitude) more accurate.

To provide independent verification of our results,
we also performed several runs using finite-difference discretizations of $G[\phi_i]$.
The Hamiltonian was discretized directly, thus ensuring a consistent discretization of $\nabla^2\phi$ and $(\nabla\phi)^2$.
We tested with both second-order accurate
\begin{equation}
 (\nabla\phi)^2dx^2 \approx G[\phi_i]_2 = \frac{1}{2}\left[(\phi_{i+1}-\phi_i)^2 + (\phi_{i-1}-\phi_i)^2\right]
\end{equation}
and fourth-order accurate
\begin{equation}
  (\nabla\phi)^2dx^2 \approx G[\phi_i]_4 = \frac{-1}{24}\left[(\phi_{i+2}-\phi_i)^2 + (\phi_{i-2}-\phi_i)^2\right] + \frac{2}{3}\left[(\phi_{i-1}-\phi_i)^2 + (\phi_{i+1}-\phi_i)^2 \right] 
\end{equation}
finite-difference stencils, where $dx$ is the lattice grid spacing.
The corresponding \laplacian\ stencils $L[\phi_i]/dx^2$ satisfying $\sum_i G[\phi_i] + \phi_i L[\phi_i] = 0$ (on periodic grids) are then the familiar second-order accurate 
\begin{equation}
  \frac{\partial^2\phi}{\partial x^2}dx^2 \approx L[\phi_i]_2 = (\phi_{i+1}-\phi_{i-1}) - 2\phi_i
\end{equation}
and fourth-order accurate
\begin{equation}
  \frac{\partial^2 \phi}{\partial x^2}dx^2 \approx L[\phi_i]_4 = \frac{-1}{12}(\phi_{i-2}+\phi_{i+2}) + \frac{4}{3}(\phi_{i-1}+\phi_{i+1}) + \frac{-5}{2}\phi_i 
\end{equation}
centered differences.

\subsection{Convergence Tests}
Here we present several tests of the convergence properties of our numerical codes.  
The combination of high-order time-integrations and spectrally accurate derivative approximations leads to a rapid convergence of both the nonlinear field evolution used to study the background dynamics and the Floquet exponents obtained by solving the perturbation equations.

Several measures of convergence for the case of the nonlinear wave equation with initial conditions as in the bottom right panel of~\figref{fig:kink_collision} are shown in~\figref{fig:nonlinear_convergence}.
In the top row we show the pointwise convergence of the solution as we vary the number of grid points $N$ (or equivalently the grid spacing) and the time-step $dt$,
 thus independently testing our Fourier spatial discretization and our Gauss-Legendre integrator.  
We consider two closely related measures,
\begin{align}
 \notag \lVert \Delta\phi^{(p)}\rVert_{L1} &\equiv N_{base}^{-1}\sum_{\{x_i\}}|\phi^{(p+1)}(x_i)-\phi^{(p)}(x_i)| \\
        \lVert\Delta\phi^{(p)}\rVert_{max}  &\equiv \max_{\{x_i\}}{|\phi^{(p+1)}(x_i)-\phi_i^{(p)}(x_i)|}
 \label{eqn:field_norms}
\end{align}
where $\phi^{(p)}$ denotes the numerical solution for the $pth$ approximation (here either the number of grid points or the time step),
and we take $N^{(p+1)}/N^{(p)} = 2 = dt^{(p)}/dt^{(p+1)}$.
To compare solutions with different spatial resolutions, we took all sums over the grid from the $N=2048$ simulation.
From the top left panel, we see the solution rapidly converges (to the level of machine precision) as we increase the spatial resolution,
exactly as we expect for a properly resolved pseudospectral code.
Comparing with the top right panel, we see that the growing error at late times is due to errors in the time-stepping rather than the spatial discretization.
One may worry about this apparent issue with the time-stepping, but this is just a demonstration that making a pointwise comparison of the fields is not necessarily the best measure of convergence.
In particular, the errors that accumulate at late time occur because we have an oscillating localized blob of field.
Small errors in the oscillation frequency then lead to errors in the instantaneous value of the field.
These errors manifest themselves as accumulating changes in the pointwise differences at late times.
As a further test of our time evolution we check the conservation of the energy density ${\rho = \langle T^{00}\rangle = \langle\frac{\dot{\phi}^2}{2}+\frac{(\nabla\phi)^2}{2}+V(\phi)\rangle}$ and field momentum ${P^x = \langle T^{0x}\rangle = -\dot{\phi}\partial_x\phi}$ for a range of time steps $dt$.
$T^{\mu\nu}$ is the energy-momentum tensor of $\phi$ and $\langle\cdot\rangle$ denotes a spatial average.
For all choices of time step, the field momentum is conserved to machine precision, 
while for $dt \le dx/5$ the energy is also conserved to machine precision.

\begin{figure}[t]
  \includegraphics[width=0.48\linewidth]{convergence_nonlinear_varyn}
  \hfill
  \includegraphics[width=0.48\linewidth]{convergence_nonlinear_varyt} \\
  \includegraphics[width=0.48\linewidth]{energy_conservation}
  \hfill
  \includegraphics[width=0.48\linewidth]{momentum_conservation}
  \caption[Convergence of one-dimensional lattice code for the double-well potential]{Convergence of our one-dimensional lattice code for the double-well potential with $\delta=1/30$, $mL=1024$ and various choices of grid spacing $dx$ and time step $dt$.  We plot the two norms defined in~\eqref{eqn:field_norms}.  The accumulating errors at late times are due to small errors in the oscillation frequency and initial phase of the oscillon that has formed at the origin.  Decreasing the time step below $dx/5$ does not lead to a decrease in this error, suggesting that it arises due to machine roundoff.  Finally, in the bottom row we demonstrate the convergence of both energy (\emph{bottom left}) and momentum (\emph{bottom right}) of the system for the same choices of $dt$ as the top right plot.
Since both (conserved) quantities are constant to very high accuracy, this demonstrates that our time-stepping procedure has converged.}
  \label{fig:nonlinear_convergence}
\end{figure}

To understand the accuracy of our Floquet exponents (and to demonstrate the great gains in accuracy obtained via a pseudospectral approach relative to a finite-difference scheme), 
~\figref{fig:floquet_convergence} shows convergence plots for the maximal Floquet exponent.
Here we can directly compare the the individual Floquet exponents, so we plot
\begin{equation}
  \Delta\mu^{(p)} \equiv |\mu_{max}^{(p+1)}-\mu_{max}^{(p)}| \, .
\end{equation}
For orientation, the top left panel shows $\mu_{max}T_{breather}$ for the choice $v=0.5$ and a range of $k_{\perp}$ values.
The remaining panels show the convergence properties of the exponents in the top left panel for various numerical schemes.
The top right panel shows the convergence rate as the time-step is varied.
As expected for a sixth-order accurate integrator, the error decreases rapidly, although not quite as quickly as for the Gauss-Legendre integrator.
Also of note is that the error decreases uniformly for all values of $k_\perp$ (except for those values that are already at machine roundoff levels)
indicating that important features such as the locations of stability bands where $\mu_{max}=0$ are not shifting around as the time-step is varied.
In the bottom row we show similar convergence plots as the number of grid points are varied while holding the simulation box size fixed.
We show results for a pseudospectral approximation, a second-order accurate finite-difference stencil, and a fourth-order accurate finite-difference stencil.
As promised, the psuedospectral method converges much more rapidly than the finite-differencing methods.
Also of note is the fact that the convergence is uniform for all $k_\perp$ with the pseudospectral approximation, while the convergence is non-uniform for the finite-difference methods.
Taking the far right 4th order chart as an example, there are several extreme spikes in the region $k_\perp^2(1+v^{-2})$ for which the difference between the $N=128$ and $N=256$ approximation is of order machine precision, but then rises to $10^{-3}$ level when comparing to the $N=512$ solution.
The ultimate source of these appearing and disappearing spikes is a slight shifting of the edges of the stability bands as the resolution is varied.
\begin{figure}[t]
  \includegraphics[width=0.48\linewidth]{{{floquet_v0.5_n64_a20}}}
  \hfill
  \includegraphics[width=0.48\linewidth]{{{evalue_convergence_n64}}} \\
  \includegraphics[width=0.96\linewidth]{{{floquet_convergence_varydx_multipanel}}}
%  \includegraphics[width=0.32\linewidth]{evalue_convergence_spectral}
%  \includegraphics[width=0.32\linewidth]{floq_convergence_discrete}
%  \includegraphics[width=0.32\linewidth]{floq_convergence_discrete4}
  \caption[Convergence plots for the largest Lyapanov exponent around a sine-Gordon breather]{Convergence of the largest Lyapanov exponent around a sine-Gordon breather with $v=0.5$ for a range of $k_{\perp}^2$ values.  The simulation lattice had length $L=58$.  For reference, the top left panel shows $\mu_{\mathrm{max}}T_{\mathrm{breather}}$ with $N=64$ and $dt=dx/20$.
These are the same parameters used along the line $v=0.5$ in the instability chart in~\figref{fig:sg_chart}.  
In the top right panel, we show the difference in the numerically determined values of $\mu_{\mathrm{max}}T_{\mathrm{breather}}$ holding the number of grid points fixed (at $N=64$) while varying the discrete time step $dt$.
A pseudospectral derivative approximation was used for each run and a sixth-order Yoshida integrator for the time evolution.  
Finally, in the bottom row we show the convergence properties as the grid spacing is decreased, using a pseudospectral (\emph{bottom left}), second-order finite-difference (\emph{bottom center}) and fourth-order finite-difference (\emph{bottom right}) approximation for the \laplacian.  In all three panels, we took $dx/dt = 20$ and used a sixth-order accurate Yoshida scheme.  
Because of the rapid convergence, the instability charts (with the exception of the $N=32$ case) are visually indistiguishable from the top left panel.}
  \label{fig:floquet_convergence}
\end{figure}
