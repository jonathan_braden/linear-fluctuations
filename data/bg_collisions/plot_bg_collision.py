import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

mpl.rc('xtick',labelsize=20)
mpl.rc('ytick',labelsize=20)

def makeplot(datfile,stamp,outfile,nlat):
    a=np.genfromtxt(datfile,usecols=[0,1,2])
    xvals=np.reshape(a[:,0],(-1,nlat))
    tvals=np.reshape(a[:,1],(-1,nlat))
    fvals=np.reshape(a[:,2],(-1,nlat))

    eps=0.1
    clevs=np.arange(-1.175,1.175+eps,eps)

    plt.contourf(xvals,tvals,fvals,clevs,cmap=plt.cm.RdYlBu,extend='both')
    cb=plt.colorbar(extend='both',orientation='vertical',ticks=[-1,-0.5,0,0.5,1])
    cb.ax.set_ylabel(r'$\frac{\phi}{\phi_0}$',fontsize=30,rotation='horizontal')
# hack to set tick sizes on colorbar
    for tk in cb.ax.get_yticklabels():
        tk.set_fontsize(18)

    plt.xlim(0.,200.)
    plt.ylim(-16.,16.)
    plt.xlabel(r'$mt$',fontsize=30)
    plt.ylabel(r'$mx$',fontsize=30)
    plt.text(190,14,stamp,fontsize=30,horizontalalignment='right',verticalalignment='top', bbox=dict(facecolor='white',alpha=1))
    plt.subplots_adjust(bottom=0.15,left=0.15)
# Make plot size nicer
    fig=plt.gcf()
    fig_width=8.5
    fig.set_size_inches(fig_width,fig_width*0.5*(np.sqrt(5.)-1.))
    fig.savefig(outfile)
    plt.show()

makeplot('field_values_v0.05.dat',r'$v=0.05$','1d_collision_v0.05.pdf',256)
makeplot('field_values_v0.2.dat',r'$v=0.2$','1d_collision_v0.2.pdf',256)
makeplot('field_values_v0.3.dat',r'$v=0.3$','1d_collision_v0.3.pdf',256)
makeplot('field_values_del1o30.dat',r'$\delta = 1/30$','1d_collision_v0_del1o30.pdf',512)
