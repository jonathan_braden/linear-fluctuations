# Use, uncomment (or add), the set of files to be compared.  Neighbours in this list will be differenced

import numpy as np
v=0.5
l=58

#infiles=["floq_v0.5_L58_a5_n128_symp6.dat","floq_v0.5_L58_a10_n128_symp6.dat","floq_v0.5_L58_a20_n128_symp6.dat"]

#infiles=["floq_v0.5_L58_a5_n64_symp6.dat","floq_v0.5_L58_a10_n64_symp6.dat","floq_v0.5_L58_a20_n64_symp6.dat","floq_v0.5_L58_a40_n64_symp6.dat","floq_v0.5_L58_a80_n64_symp6.dat"]
#name=[r"$dt^{(p)}=dx/5$",r"$dt^{(p)}=dx/10$",r"$dt^{(p)}=dx/20$",r"$dt^{(p)}=dx/40$"]
#figname='evalue_convergence_n64.pdf'

infiles=["floq_v0.5_L58_a20_n32_symp6.dat","floq_v0.5_L58_a20_n64_symp6.dat","floq_v0.5_L58_a20_n128_symp6.dat","floq_v0.5_L58_a20_n256_symp6.dat","floq_v0.5_L58_a20_n512_symp6.dat"]
name=[r"$N^{(p)}=32$",r"$N^{(p)}=64$",r"$N^{(p)}=128$",r"$N^{(p)}=256$"]
figname='evalue_convergence_spectral.pdf'

if (len(name)+1 != len(infiles)):
    print "Error, incorrect number of labels for input files"

fields=[]
muvals=[]
for i in range(len(infiles)):
    fname=infiles[i]
    a=np.genfromtxt(fname,usecols=[1,2])
    kvals=a[:,0]
    muvals.append(a[:,1])

diffs=[]
for i in range(len(muvals)-1):
    diffs.append(muvals[i+1]-muvals[i])

import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

kvals=kvals*(1.+v**2)/v**2
for i in range(len(diffs)):
    plt.plot(kvals,np.abs(diffs[i]), linewidth=2, label=name[i])

plt.xlabel(r'$k_{\perp}^2(1+v^2)/v^2$',fontsize=30)
plt.ylabel(r'$|\mu_{max}^{(p+1)}-\mu_{max}^{(p)}|$', fontsize=30)
plt.legend(loc='lower left', bbox_to_anchor=(-0.02,-0.02),fontsize=22)

plt.yscale('log')
plt.ylim(1.e-18,10.)
plt.xlim((0.,12.5))
plt.subplots_adjust(bottom=0.15,left=0.17)
plt.savefig(figname)
plt.show()
