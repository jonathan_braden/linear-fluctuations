import numpy as np
v=0.5
l=58

infile="floq_v0.5_L58_a20_n64_symp6.dat"

a=np.genfromtxt(infile,usecols=[1,2])
kvals=a[:,0]
kvals=kvals*(1.+v**2)/v**2
muvals=a[:,1]

import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

plt.plot(kvals,2.*muvals, linewidth=2)
plt.ylabel(r"$\mu_{\mathrm{max}}T_{\mathrm{breather}}$",fontsize=30)
plt.xlabel(r"$k_{\perp}^2(1+v^2)/v^2$",fontsize=30)
plt.subplots_adjust(bottom=0.15)

plt.savefig('floquet_v0.5_n64_a20.pdf')
plt.show()
