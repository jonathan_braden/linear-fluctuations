import numpy as np

infiles=["en_l1024_n2048_dt0.4.dat","en_l1024_n2048_dt0.2.dat", "en_l1024_n2048_dt0.1.dat","en_l1024_n2048_dt0.05.dat","en_l1024_n2048_dt0.025.dat"]#,"en_l1024_n2048_dt0.0125.dat"]
labels=[r'$dt=4dx/5$',r'$dt=2dx/5$',r'$dt=dx/5$',r'$dt=dx/10$',r'$dt=dx/20$']

tvals=[]
evals=[]
pvals=[]
for fname in infiles:
    a=np.genfromtxt(fname,usecols=[0,1,5])
    etmp=a[:,1]
    print "initial energy is ",etmp[0]
    etmp=etmp-etmp[0]
    evals.append(etmp)
    tvals.append(a[:,0])
    pvals.append(a[:,2])

import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

for i in range(len(evals)):
    plt.plot(tvals[i],np.abs(evals[i]),label=labels[i])

plt.xlabel(r'$mt$',fontsize=30)
plt.ylabel(r'$|\rho-\rho_{init}|$',fontsize=30)
plt.yscale('log')
plt.xlim(0,400)
plt.legend(fontsize=22,bbox_to_anchor=(0,0,1.1,1.1),loc='upper right')
plt.subplots_adjust(bottom=0.15,left=0.16)
plt.savefig('energy_conservation.pdf')
plt.show()

for i in range(len(pvals)):
    plt.plot(tvals[i],np.abs(pvals[i]),label=labels[i])

plt.xlabel(r'$mt$',fontsize=30)
plt.ylabel(r'$\left|\langle\dot{\phi}\partial_x\phi\rangle\right|$',fontsize=30)
plt.xlim(0,400)
plt.legend(loc='upper left', fontsize=22)
plt.subplots_adjust(bottom=0.15)
plt.savefig('momentum_conservation.pdf')
plt.show()
