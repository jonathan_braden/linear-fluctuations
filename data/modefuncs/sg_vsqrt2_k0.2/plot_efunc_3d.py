import numpy as np
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)
#mpl.rc('ztick',labelsize=18)

infile = "eigenfunction_vsqrt2_n256_k0.2_10period.dat"

vval=1./(2.**0.5-1.)
period=np.pi*(1.+vval**2)**0.5/vval
xrnge=(-20,20)
nlat=256
nperiod=256

a=np.genfromtxt(infile,usecols=[0])
x=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[1])
t=np.reshape(a,(-1,nlat))
t=t/period
a=np.genfromtxt(infile,usecols=[2])
fld=np.reshape(a,(-1,nlat))

# Keep only part of the output
x=x[:2*nperiod,nlat/2-32:nlat/2+32]
t=t[:2*nperiod,nlat/2-32:nlat/2+32]
fld=fld[:2*nperiod,nlat/2-32:nlat/2+32]

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

fig=plt.figure()
ax=fig.gca(projection='3d')

surf=ax.plot_surface(x,t,fld,rstride=16,cstride=8,cmap=plt.cm.RdBu,alpha=0.7,linewidth=0.,shade=True,vmin=-0.4,vmax=0.4)#,norm=mpl.colors.SymLogNorm(linthresh=1.,vmin=-2.,vmax=2.))
cb=plt.colorbar(surf,ticks=[-0.4,-0.2,0,0.2,0.4])

ax.set_xlabel(r'$x$',fontsize=24)
ax.set_ylabel(r'$t/T_b}$',fontsize=24)
ax.set_yticks([0,1,2])
ax.set_zticks([-0.5,0,0.5])
ax.set_ylim(0.,2.)
ax.set_zlabel(r'$\delta\phi$',fontsize=24)
ax.set_autoscale_on(False)
#ax.set_zscale('symlog')
plt.savefig('sg_vsqrt2_k0.2_modefunc3d.png')
plt.savefig('sg_vsqrt2_k0.2_modefunc3d.pdf')
plt.show()
