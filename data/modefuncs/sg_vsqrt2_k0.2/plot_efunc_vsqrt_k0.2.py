import sys
sys.path.insert(0,'/home/jbraden/Documents/python_scripts/')

import myfileutils as myf
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

golden_ratio=0.5*(np.sqrt(5.)-1.)
fig_width=8.5
fig_height=golden_ratio*fig_width

mpl.rc('xtick',labelsize=20)
mpl.rc('ytick',labelsize=20)

infile="eigenfunction_vsqrt2_n256_k0.2_10period.dat"

vval = 1./(2.**0.5-1.) 
kperp=0.2**0.5

period = np.pi*(1.+vval**2)**0.5/vval  # period of effective mass, not bg
vals=myf.read_timeblock(infile,[0,1,2,3,4])
omega=np.pi/period

tvals=[]
fvals=[]
pvals=[]
npartf=[]
npartp=[]
for i in range(len(vals)):
    tvals.append(vals[i][1])
    fvals.append(vals[i][2])
    pvals.append(vals[i][3])

tvals=np.array(tvals)
tvals=tvals.T
tvals = tvals / 2. / period  # normalize to breather period
fvals=np.array(fvals)
pvals=np.array(pvals)
npartf=fvals**2
npartp=pvals**2

npartx=0.5*(npartp + omega**2*npartf) / omega

npart=[]
for i in range(len(npartx)):
    npart.append(np.sum(npartx[i]))

plt.plot(tvals[0],npart, linewidth=2)
plt.ylabel(r'$n^{\omega_{breather}}_{\mathrm{eff}}$',fontsize=30)
plt.xlabel(r'$t/T_{\mathrm{breather}}$',fontsize=30)
plt.subplots_adjust(left=0.17,bottom=0.15)
plt.xlim(0,10)
plt.yscale('log')

fig=plt.gcf()
fig.set_size_inches(fig_width,fig_height)
plt.savefig('npart_vsqrt2_k0.2.pdf')
plt.show()
