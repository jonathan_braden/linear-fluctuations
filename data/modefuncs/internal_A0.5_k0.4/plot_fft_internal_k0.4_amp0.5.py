import numpy as np

#infile = "eigenfunction_internal_A0.5_k0.4_L125_2period_n256_spectral.dat"
#nlat=256

infile = "eigenfunction_internal_A0.5_k0.4_L125_2period_n512_spectral.dat"
nlat=512

period=2.*np.pi
floquet=0.73882793586629958/period
xrnge=(-20,20)

a=np.genfromtxt(infile,usecols=[0])
x=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[1])
t=np.reshape(a,(-1,nlat))
t=t
t2=t.T
a=np.genfromtxt(infile,usecols=[2])
f=np.reshape(a,(-1,nlat))
f2=f.T

a=np.genfromtxt(infile,usecols=[4])
m2eff=np.reshape(a,(-1,nlat))

fn=np.exp(-floquet*t)*f
fn2=fn.T
# x,t,f are now positions, times, and function values as a function of x in first column, versus time

# Now perform the FFTs
ntime=len(f)
ft=np.fft.rfft(fn,(ntime-1),axis=0)
ft2=ft.T

# Normalize the spectrum to the RMS at that location
flucpow=[]
fnorm=[]
fmax=[]
for i in range(len(ft2)):
    fpow=np.sum(abs(ft2[i])**2)
    flucpow.append(fpow)
    fnorm.append(ft2[i]/np.sqrt(fpow))
    fmax.append(np.max(abs(fnorm[i])))

fnorm=np.array(fnorm)
fnormt=fnorm.T
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

plt.contourf(x,t/period,1.5*m2eff-1.,15,cmap=plt.cm.PuBu_r)
plt.xlabel(r'$\sqrt{3/2}mx$',fontsize=30)
plt.ylabel(r'$t/T_{shape}$',fontsize=30)
plt.xlim(-15.,15.)
cb=plt.colorbar()
cb.set_label(r'$\partial_{\phi\phi}V$',fontsize=26)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('internal_a0.5_m2eff.png')
plt.show()

ax=plt.subplot(1,1,1)
ax.plot(x[0],np.abs(ft[1]),label=r'$\omega = \pi T_{\mathrm{shape}}^{-1}$',linewidth=2)
ax.plot(x[0],np.abs(ft[3]),label=r'$\omega = 3\pi T_{\mathrm{shape}}^{-1}$',linewidth=2)
ax.plot(x[0],np.abs(ft[5]),label=r'$\omega = 5\pi T_{\mathrm{shape}}^{-1}$',linewidth=2)
ax.set_yticks([])
plt.xlabel(r'$\sqrt{3/2}mx$',fontsize=30)
plt.ylabel(r'$|\delta\tilde{\phi}_{\omega_i}(x)|$',fontsize=30)
plt.legend(loc='upper right',fontsize=20,bbox_to_anchor=(0,0,1.1,1.1))
plt.xlim(xrnge)
#plt.ylim(0,80)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('fourier_amp_internal_A0.5_k0.4.pdf')
plt.show()

# Now make whatever plots I want
plt.plot(x[0],abs(fnormt[1]),label=r'$\omega = \pi T_{\mathrm{shape}}^{-1}$',linewidth=2)
plt.plot(x[0],abs(fnormt[3]),label=r'$\omega = 3\pi T_{\mathrm{shape}}^{-1}$',linewidth=2)
plt.plot(x[0],abs(fnormt[5]),label=r'$\omega = 5\pi T_{\mathrm{shape}}^{-1}$',linewidth=2)
plt.xlabel(r'$\sqrt{3/2}mx$',fontsize=30)
plt.ylabel(r'$|\delta\tilde{\phi}_{\omega}(x)|/\sigma_{\omega}(x)$',fontsize=30)
plt.xlim(xrnge)
plt.ylim(0,1.1)
plt.legend(loc='center right',fontsize=20,bbox_to_anchor=(0,0,1.1,1))
plt.subplots_adjust(bottom=0.15,left=0.17)
plt.savefig('fourier_power_internal_A0.5_k0.4.pdf')
plt.show()

# Before plotting the angles, turn them into continuous variables
theta=np.zeros([3,len(ft2)])
theta[0,:]=np.angle(ft2[:,1])
theta[1,:]=np.angle(ft2[:,3])
theta[2,:]=np.angle(ft2[:,5])

# A better plan here would be to do some interpolations to determine the new x's
thetanew=[]
xnew=[]
for i in range(len(theta)):
    pos=np.where(np.abs(np.diff(theta[i])) > np.pi)[0]
    thetanew.append(np.insert(theta[i],pos+1,np.nan))
    xnew.append(np.insert(x[0],pos+1,np.nan))

plt.plot(xnew[0],thetanew[0],label=r'$\omega = \pi T_{\mathrm{breather}}^{-1}$')
plt.plot(xnew[1],thetanew[1],label=r'$\omega = 3\pi T_{\mathrm{breather}}^{-1}$')
plt.plot(xnew[2],thetanew[2],label=r'$\omega = 5\pi T_{\mathrm{breather}}^{-1}$')
plt.xlabel(r'$x$',fontsize=30)
#plt.ylabel(r'$\mathrm{arg}(\delta\tilde{\phi}_{\omega})$',fontsize=16)
plt.ylabel(r'$\theta_{\omega_i}(x)$',fontsize=30)
plt.ylim(-np.pi,np.pi)
plt.xlim(xrnge)
plt.legend(loc='center left')

plt.show()

def continuous_angles(y):
    mid=len(y)/2-1
    wind=np.zeros(len(y))
    d=np.diff(y)
    winding_num=0
    for i in range(len(d)):
        if (d[i]>np.pi):
            winding_num-=1
        elif (d[i]<-np.pi):
            winding_num+=1
        wind[i+1]=winding_num

    wind=wind-wind[mid]
    return y+2.*np.pi*wind

thetanew=[]
xnew=[]
plt.plot(x[0],continuous_angles(theta[0]),label=r'$\omega = \pi T_{\mathrm{shape}}^{-1}$',linewidth=2)
plt.plot(x[0],continuous_angles(theta[1]),label=r'$\omega = 3\pi T_{\mathrm{shape}}^{-1}$',linewidth=2)
plt.plot(x[0],continuous_angles(theta[2]),label=r'$\omega = 5\pi T_{\mathrm{shape}}^{-1}$',linewidth=2)
plt.xlabel(r'$\sqrt{3/2}mx$',fontsize=30)
#plt.ylabel(r'$\mathrm{arg}(\delta\tilde{\phi}_{\omega})$',fontsize=16)
plt.ylabel(r'$\theta_{i}(x)$',fontsize=30)
plt.xlim(xrnge)
plt.ylim(-5*np.pi,2.*np.pi)
plt.legend(loc='upper right',fontsize=20,bbox_to_anchor=(0,0,1.1,1.1))
plt.subplots_adjust(bottom=0.15,left=0.17)
plt.savefig('fourier_arg_internal_A0.5_k0.4.pdf')
plt.show()

