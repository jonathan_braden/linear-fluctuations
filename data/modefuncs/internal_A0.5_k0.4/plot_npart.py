import numpy as np

golden_ratio=0.5*(np.sqrt(5.)-1.)
fig_width=8.5
fig_height=golden_ratio*fig_width

#infile="eigenfunction_internal_A0.5_k0.4_L125_2period_n512_spectral.dat"
#infile="eigenfunction_internal_A0.5_k0.4_L125_10period_n512_spectral.dat"
infile="eigenfunction_internal_A0.5_k0.4_L62_10period_n512_spectral.dat"
nlat=512
period=2.*np.pi
omega=np.pi/period

a=np.genfromtxt(infile,usecols=[0,1,2,3])
x=np.reshape(a[:,0],(-1,nlat))
t=np.reshape(a[:,1],(-1,nlat))
phi=np.reshape(a[:,2],(-1,nlat))
dphi=np.reshape(a[:,3],(-1,nlat))

npart=0.5/omega*(dphi**2+omega**2*phi**2)
ntot=[]
for i in range(len(npart)):
    ntot.append(np.sum(npart[i]))

import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import axes3d

mpl.rc('xtick',labelsize=20)
mpl.rc('ytick',labelsize=20)

xmin=nlat/2-128
xmax=nlat/2+128
fig=plt.figure()
ax=fig.gca(projection='3d')
surf=ax.plot_surface(x[:257,xmin:xmax],t[:257,xmin:xmax]/period,phi[:257,xmin:xmax],rstride=16,cstride=16,cmap=plt.cm.RdBu, alpha=0.7, linewidth=0.1,vmin=-.5,vmax=.5)
cb=plt.colorbar(surf,ticks=[-1,-0.5,0,0.5,1])
ax.set_xlabel(r'$mx/\sqrt{3}$',fontsize=24)
ax.set_ylabel(r'$t/T_{shape}$',fontsize=24)
ax.set_zlabel(r'$\delta\phi$',fontsize=24)
ax.set_xticks([-15,0,15])
ax.set_yticks([0,2])
ax.set_zticks([-0.5,0,0.5])
ax.set_xlim(-20,20)
ax.set_ylim(0,2)
plt.savefig('internal_a0.5_k0.4_efunc.pdf')
plt.savefig('internal_a0.5_k0.4_efunc.png')
plt.show()

plt.plot(t[:,0]/period,ntot,linewidth=1.5)
plt.xlabel(r'$t/T_{shape}$',fontsize=30)
plt.ylabel(r'$n_{eff}^{\omega_{shape}}$',fontsize=30)
plt.xlim(0,10)
plt.yscale('log')
plt.subplots_adjust(left=0.17,bottom=0.15)

fig=plt.gcf()
#fig.set_size_inches(fig_width,fig_height)
fig.savefig('internal_a0.5_k0.4_npart.pdf')
plt.show()
