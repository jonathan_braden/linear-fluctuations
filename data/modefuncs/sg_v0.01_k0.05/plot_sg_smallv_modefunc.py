import numpy as np

#infile = "efunc_sg_v0.01_k0.05_n512.dat"
#nlat=512

infile = "efunc_sg_v0.01_k0.05_1024perperiod.dat"
nlat=256
k2=0.05
period=2.*np.pi*(1.+0.01**2)**0.5/0.01

a=np.genfromtxt(infile,usecols=[0,1,2,3,4])

x=a[:,0]
x=np.reshape(x,(-1,nlat))
t=a[:,1]
t=np.reshape(t,(-1,nlat))
t=t/period
phi=a[:,2]
phi=np.reshape(phi,(-1,nlat))
dphi=a[:,3]
dphi=np.reshape(dphi,(-1,nlat))
m2eff=a[:,4]
m2eff=np.reshape(m2eff,(-1,nlat))

maxval_l=[]
maxval_r=[]
massmin=[]
for i in range(len(m2eff)):
    tmp=m2eff[i,:nlat/2]
    ind_l=np.argmin(tmp)
    if (abs(m2eff[i,ind_l]-m2eff[i,0])<1.e-5):
        ind_l=nlat/2-1
    tmp=m2eff[i,nlat/2:]
    ind_r=np.argmin(tmp)+nlat/2
    if (abs(m2eff[i,ind_r]-m2eff[i,0])<1.e-5):
        ind_r=nlat/2
    maxval_l.append(phi[i,ind_l])
    maxval_r.append(phi[i,ind_r])
    massmin.append(m2eff[i,ind_l])

npart=k2*phi**2+dphi**2
np_left=[]
np_right=[]
np_tot=[]
for i in range(len(npart)):
    np_tot.append(np.sum(npart[i]))
    np_left.append(np.sum(npart[i][0:nlat/2-1]))
    np_right.append(np.sum(npart[i][nlat/2:nlat]))

import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=20)
mpl.rc('ytick',labelsize=20)

fldmax=np.max(np.abs(maxval_l))
plt.plot(t[:,0],np.array(maxval_l)/500.,'b',linewidth=2,label=r'$\delta\phi_{wall}$')
plt.plot(t[:,0],np.array(massmin),'g',linewidth=2,label=r'$\partial_{\phi\phi}V_{min}$')
plt.xlabel(r'$t/T_{\mathrm{breather}}$',fontsize=30)
plt.ylabel(r'$\delta\phi_{\mathrm{wall}}$',fontsize=30)
plt.legend(loc='upper left',fontsize=24)
plt.xlim(0,1.5)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('sg_mode_onwall_v0.01_k0.05.pdf')
plt.show()

plt.plot(t[:,0],np_tot,linewidth=2)
plt.xlabel(r'$t/T_{\mathrm{breather}}$',fontsize=30)
plt.ylabel(r'$n^{\omega_{bound}}_{eff}$',fontsize=30)
plt.yscale('log')
plt.subplots_adjust(bottom=0.15,left=0.16)
plt.savefig('npart_v0.01_k0.05_short.pdf')
plt.show()

clevs=np.linspace(-0.6,0.6,21)
plt.contourf(x,t,phi,clevs,cmap=plt.cm.RdBu,extend='both')
plt.xlim(-15,15)
plt.xlabel(r'$x$',fontsize=30)
plt.ylabel(r'$t/T_{\mathrm{breather}}$',fontsize=30)
cb=plt.colorbar(orientation='vertical')
cb.ax.set_ylabel(r'$\phi$',fontsize=30)
plt.show()
