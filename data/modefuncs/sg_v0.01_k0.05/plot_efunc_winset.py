import numpy as np

golden_ratio=0.5*(np.sqrt(5.)-1.)
fig_width=8.5
fig_height=fig_width*golden_ratio

infile="eigenfunction_L538_n1024.dat"
nlat=1024

a=np.genfromtxt(infile,usecols=[0,1,2])
x=a[:,0]
x=x-x[nlat/2-1]
phi=a[:,1]
dphi=a[:,2]

import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rc('xtick',labelsize=20)
mpl.rc('ytick',labelsize=20)

plt.plot(x,phi,linewidth=2,label=r'$\delta\phi_{k_\perp}$')
plt.plot(x,dphi,linewidth=2,label=r'$\delta\dot{\phi}_{k_\perp}$')
plt.xlabel(r'$x$',fontsize=30)
plt.ylabel(r'$\delta\phi_{k_\perp},\delta\dot{\phi}_{k_\perp}$',fontsize=30)
plt.xlim(-100,100)
plt.legend(loc='upper right',fontsize=30,bbox_to_anchor=(0,0,1.1,1.1))
plt.subplots_adjust(bottom=0.15,left=0.2)

fig=plt.gcf()
fig.set_size_inches(fig_width,fig_height)
fig.savefig('sg_mode_initial_v0.01_k0.05.pdf')
plt.show()
