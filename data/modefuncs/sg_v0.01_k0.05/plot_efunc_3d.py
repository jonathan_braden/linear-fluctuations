import numpy as np
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)
#mpl.rc('ztick',labelsize=18)

infile = "efunc_sg_v0.01_k0.05_1024perperiod.dat"

vval=0.01
period=2.*np.pi*(1.+0.01**2)**0.5/0.01
xrnge=(-20,20)
nlat=256
nperiod=1024

a=np.genfromtxt(infile,usecols=[0])
x=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[1])
t=np.reshape(a,(-1,nlat))
t=t/period
a=np.genfromtxt(infile,usecols=[2])
fld=np.reshape(a,(-1,nlat))

# Keep only part of the output
x=x[:nperiod,nlat/2-80:nlat/2+80]
t=t[:nperiod,nlat/2-80:nlat/2+80]
fld=fld[:nperiod,nlat/2-80:nlat/2+80]

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

fig=plt.figure()
ax=fig.gca(projection='3d')

surf=ax.plot_surface(x,t,fld,rstride=16,cstride=8,cmap=plt.cm.RdBu,alpha=0.7,linewidth=0.,shade=True,vmin=-1,vmax=1.,norm=mpl.colors.SymLogNorm(linthresh=0.01,vmin=-1.,vmax=1.))
cb=plt.colorbar(surf,ticks=[-2,-1,-0.5,-0.1,0,0.1,0.5,1,2])

ax.set_xlabel(r'$x$',fontsize=24)
ax.set_ylabel(r'$t/T_b}$',fontsize=24)
ax.set_yticks([0,0.5])
ax.set_zticks([-2,-1,0,1,2])
ax.set_ylim(0.,0.5)
ax.set_zlabel(r'$\delta\phi$',fontsize=24)
ax.set_autoscale_on(False)
#ax.set_zscale('symlog')
plt.savefig('sg_v0.01_k0.05_modefunc3d.png')
plt.savefig('sg_v0.01_k0.05_modefunc3d.pdf')
plt.show()
