import numpy as np
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

fsize=26

infile="floquet_internal_L72w_n256.dat"
fsize=101
numk=26
outfile="internal_chart_spectral.pdf"

a=np.genfromtxt(infile,usecols=[0,1,2])
vvals=a[:,0]
vvals=np.reshape(vvals,(-1,numk))
vvals=vvals.T

kvals=a[:,1]
kvals=np.reshape(kvals,(-1,numk))
kvals=kvals.T

muvals=a[:,2]
muvals=np.reshape(muvals,(-1,numk))
muvals=muvals.T

import matplotlib.pyplot as plt
plt.contourf(vvals,kvals,muvals,31,cmap=plt.cm.OrRd)
cb=plt.colorbar(orientation='vertical')
cb.ax.set_ylabel(r'$\mu_{max}T_{shape}$',fontsize=30)
plt.ylabel(r'$k_\perp^2$',fontsize=30)
plt.xlabel(r'$A_{shape}$',fontsize=30)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig(outfile)
plt.show()
