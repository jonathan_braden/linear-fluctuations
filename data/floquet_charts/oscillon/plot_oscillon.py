import numpy as np
import matplotlib as mpl

mpl.rc('xtick',labelsize=16)
mpl.rc('ytick',labelsize=16)

fsize=26

#infile="floquet_oscillon_n64.dat"
#outfile="floquet_oscillon.pdf"
#numk=36
infile="floquet_oscillon_order2_n64.dat"
outfile="floquet_oscillon_order2.pdf"
numk=201

a=np.genfromtxt(infile,usecols=[0,1,2])
vvals=a[:,0]
vvals=np.reshape(vvals,(-1,numk))
vvals=vvals.T

kvals=a[:,1]
kvals=np.reshape(kvals,(-1,numk))
kvals=kvals.T

muvals=a[:,2]
muvals=np.reshape(muvals,(-1,numk))
muvals=muvals.T

import matplotlib.pyplot as plt
plt.contourf(vvals,kvals,muvals,31,cmap=plt.cm.OrRd)
cb=plt.colorbar(orientation='vertical')
cb.ax.set_ylabel(r'$\mu_{max}T_{oscillon}$',fontsize=fsize)
plt.ylabel(r'$k_\perp^2$',fontsize=fsize)
plt.xlabel(r'$\epsilon$',fontsize=fsize)
plt.subplots_adjust(bottom=0.12,left=0.15)
plt.savefig(outfile)
plt.show()
