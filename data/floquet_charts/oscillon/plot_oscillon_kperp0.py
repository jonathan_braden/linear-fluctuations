import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

infile=["floquet_oscillon_n64.dat","floquet_oscillon_order2_n64.dat"]#""
numk=[36,201]

vvals=[]
mu_vals=[]
for i in range(len(infile)):
    a=np.genfromtxt(infile[i],usecols=[0,1,2])
    v=np.reshape(a[:,0],(-1,numk[i]))
    mu=np.reshape(a[:,2],(-1,numk[i]))
    vvals.append(v[:,0])
    mu_vals.append(mu[:,0])

plt.plot(vvals[0],mu_vals[0],linewidth=2,label=r'First Order')
plt.plot(vvals[1],mu_vals[1],linewidth=2,label=r'Second Order')
plt.legend(fontsize=22,loc='upper left')
plt.xlabel(r'$\epsilon$',fontsize=30)
plt.ylabel(r'$\mu_{max}T_{oscillon}$',fontsize=30)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('floquet_oscillon_kperp0.pdf')
plt.show()
