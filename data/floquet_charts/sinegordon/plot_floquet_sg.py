import numpy as np
import matplotlib as mpl

golden_ratio=0.5*(np.sqrt(5.)-1.)
fig_width=8.5
fig_height=golden_ratio*fig_width

fsize=30

mpl.rc('xtick',labelsize=20)
mpl.rc('ytick',labelsize=20)

#infile="floquet_sg_n64_rat1e-5.dat"
#infile="floquet_smallv_n64_sg.dat"
infile="floquet_sinegordon_spectral_n64.dat"
numk=201

a=np.genfromtxt(infile,usecols=[0,1,2])
vvals=a[:,0]
vvals=np.reshape(vvals,(-1,numk))
vvals=vvals.T

kvals=a[:,1]
kvals=np.reshape(kvals,(-1,numk))
kvals=kvals.T

muvals=a[:,2]
muvals=np.reshape(muvals,(-1,numk))
muvals=muvals.T

# Redefine variables into appropriate coordinates
kvals=kvals*(1.+vvals**2)/vvals**2

import matplotlib.pyplot as plt
plt.contourf(1./vvals,kvals,2.*muvals,31,cmap=plt.cm.OrRd)
cb=plt.colorbar(orientation='vertical')
cb.ax.set_ylabel(r'$\mu_{max}T_{breather}$',fontsize=fsize)
plt.ylabel(r'$k_\perp^2(1+v^{-2})$',fontsize=fsize)
plt.xlabel(r'$v^{-1}$',fontsize=fsize)
plt.subplots_adjust(bottom=0.15,left=0.15)

fig = plt.gcf()
fig.set_size_inches(fig_width,fig_height)

fig.savefig('sg_chart.pdf')
plt.show()
