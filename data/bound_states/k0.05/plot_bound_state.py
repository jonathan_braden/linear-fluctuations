import numpy as np

infile = "bound_state_k0.05_10period_a20_n1024.dat"
nlat=1024
k2=0.05
period=2.*np.pi*(1.+0.01**2)**0.5/0.01

a=np.genfromtxt(infile,usecols=[0,1,2,3,6])

x=a[:,0]
x=np.reshape(x,(-1,nlat))
t=a[:,1]
t=np.reshape(t,(-1,nlat))
t=t/period
phi=a[:,2]
phi=np.reshape(phi,(-1,nlat))
dphi=a[:,3]
dphi=np.reshape(dphi,(-1,nlat))
m2eff=a[:,4]
m2eff=np.reshape(m2eff,(-1,nlat))

maxval_l=[]
maxval_r=[]
massmin=[]
for i in range(len(m2eff)):
    tmp=m2eff[i,:nlat/2]
    ind_l=np.argmin(tmp)
    if (abs(m2eff[i,ind_l]-m2eff[i,0])<1.e-5):
        ind_l=nlat/2-1
    tmp=m2eff[i,nlat/2:]
    ind_r=np.argmin(tmp)+nlat/2
    if (abs(m2eff[i,ind_r]-m2eff[i,0])<1.e-5):
        ind_r=nlat/2
    maxval_l.append(phi[i,ind_l])
    maxval_r.append(phi[i,ind_r])
    massmin.append(m2eff[i,ind_l])

npart=k2*phi**2+dphi**2
np_left=[]
np_right=[]
np_tot=[]
for i in range(len(npart)):
    np_tot.append(np.sum(npart[i]))
    np_left.append(np.sum(npart[i][0:nlat/2-1]))
    np_right.append(np.sum(npart[i][nlat/2:nlat]))

import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=20)
mpl.rc('ytick',labelsize=20)

plt.plot(t[:,0],maxval_l,'b',linewidth=1.5,label=r'$\delta\phi_{wall,l}$')
plt.plot(t[:,0],maxval_r,'r-.',linewidth=1.5,label=r'$\delta\phi_{wall,r}$')
plt.plot(t[:,0],massmin,'g',linewidth=2)#,label=r'$\partial_{\phi\phi}V{min}$')
plt.xlabel(r'$t/T_{\mathrm{breather}}$',fontsize=30)
plt.ylabel(r'$\delta\phi_{\mathrm{wall}}$',fontsize=30)
plt.legend(fontsize=28,loc='upper left',bbox_to_anchor=(0,0,1,1.1))
plt.yscale('symlog',linthreshy=1)
plt.xlim(0,2)
plt.ylim(-1.e7,1.e7)
plt.subplots_adjust(bottom=0.16,left=0.17)
plt.savefig('field_wall_k0.05.pdf')
plt.show()

plt.plot(t[:,0],np_tot,linewidth=2,label=r'$n_{eff}^{tot}$')
plt.plot(t[:,0],np_left,'g--',linewidth=2,label=r'$n_{eff}^{left}$')
plt.plot(t[:,0],np_right,'r-.',linewidth=2,label=r'$n_{eff}^{right}$')
plt.xlabel(r'$t/T_{\mathrm{breather}}$',fontsize=30)
plt.ylabel(r'$n_{eff}$',fontsize=30)
plt.yscale('log')
plt.legend(loc='lower right',fontsize=30,bbox_to_anchor=(0,0,1.1,1))
plt.subplots_adjust(left=0.17,bottom=0.16)
plt.savefig('npart_k0.05.pdf')
plt.show()

#plt.plot(x[0,:],phi[120,:],linewidth=1.5,label=r'$t=15T_{breather}/32$')
#plt.plot(x[0,:],phi[128,:],linewidth=1.5,label=r'$t=T_{breather}/2$')
#plt.plot(x[0,:],phi[136,:],linewidth=1.5,label=r'$t=17T_{breather}/2$')
#plt.plot(x[0,:],phi[144,:],linewidth=1.5,label=r'$t=18T_{breather}/2$')
#plt.plot(x[0,:],phi[152,:],linewidth=1.5,label=r'$t=19T_{breather}/2$')
import matplotlib.colors as colors
import matplotlib.cm as cmx

t_plots=[64,80,96,112,128,144,160,176,192]
mycmap=plt.get_cmap('brg')
cNorm=colors.Normalize(vmin=0,vmax=len(t_plots)-1)
scalarMap=cmx.ScalarMappable(norm=cNorm,cmap=mycmap)

for i in range(len(t_plots)):
    tcur = t_plots[i]
    curcol=scalarMap.to_rgba(i)
    plt.plot(x[0,:],npart[tcur,:],linewidth=1.5,color=curcol,label=r'$t={:.2f}T_b$'.format(t[tcur,0]) )

plt.legend(fontsize=26,loc='upper right',bbox_to_anchor=(0,0,1.1,1.1))
plt.xlim(-30,50)
plt.xlabel(r'$mx$',fontsize=30)
plt.ylabel(r'$\delta\dot{\phi}^2+k_\perp^2\delta\phi^2$',fontsize=30)
#plt.yscale('log')
plt.subplots_adjust(bottom=0.16,left=0.17)
plt.savefig('npart_dist_k0.05.pdf')
plt.show()

#clevs=np.linspace(-0.6,0.6,21)
#plt.contourf(x,t,phi,clevs,cmap=plt.cm.RdBu,extend='both')
#plt.xlim(-15,15)
#plt.xlabel(r'$x$',fontsize=16)
#plt.ylabel(r'$t/T_{\mathrm{breather}}$',fontsize=16)
#cb=plt.colorbar(orientation='vertical')
#cb.ax.set_ylabel(r'$\phi$',fontsize=16)
#plt.show()
