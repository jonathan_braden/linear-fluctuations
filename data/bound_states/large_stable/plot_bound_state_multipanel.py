import numpy as np
import gc as gc  # garbage collector
import matplotlib as mpl

infile = "bound_state_v0.01_k0.83_a20_n4096.dat"
nlat=4096
nkeep=256
nbox=100  # keep +/- 25 around collision
k2=0.83
period=2.*np.pi*(1.+0.01**2)**0.5/0.01

# The file is huge, so don't do all the reads at one
a=np.genfromtxt(infile,usecols=[0])
x=np.reshape(a,(-1,nlat))
xline=np.array(x[0])
x=x[:,nlat/2-nkeep:nlat/2+nkeep]
a=[]
gc.collect()

a=np.genfromtxt(infile,usecols=[1])
t=np.reshape(a,(-1,nlat))
t=t/period
t=t[:,nlat/2-nkeep:nlat/2+nkeep]
tline=np.array(t[:,0])
a=[]
gc.collect()

a=np.genfromtxt(infile,usecols=[2])
phi=np.reshape(a,(-1,nlat))

a=np.genfromtxt(infile,usecols=[3])
dphi=np.reshape(a,(-1,nlat))

a=np.genfromtxt(infile,usecols=[6])
m2eff=np.reshape(a,(-1,nlat))

maxval_l=[]
maxval_r=[]
massmin=[]
for i in range(len(m2eff)):
    tmp=m2eff[i,:nlat/2]
    ind=np.argmin(tmp)
    if (abs(m2eff[i,ind]-m2eff[i,0])<1.e-5):
        ind=nlat/2-1
    maxval_l.append(phi[i,ind])
    tmp=m2eff[i,nlat/2:nlat]
    ind=np.argmin(tmp)
    if (abs(m2eff[i,ind+nlat/2]-m2eff[i,0])<1.e-5):
        ind=nlat/2-1
    maxval_r.append(phi[i,ind+nlat/2])
    massmin.append(m2eff[i,ind+nlat/2])

npart=k2*phi**2+dphi**2
np_left=[]
np_right=[]
np_tot=[]
np_box=[]
for i in range(len(npart)):
    np_tot.append(np.sum(npart[i]))
    np_box.append(np.sum(npart[i,nlat/2-nbox:nlat/2+nbox]))
    np_left.append(np.sum(npart[i][nlat/2-nbox:nlat/2]))
    np_right.append(np.sum(npart[i][nlat/2:nlat/2+nbox]))

import matplotlib.pyplot as plt

mpl.rc('xtick',labelsize=20)
mpl.rc('ytick',labelsize=20)

fig, axi = plt.subplots(nrows=3,sharex=True)
axi[0].plot(tline,maxval_l,'b',linewidth=2,label=r'$\delta\phi_{wall,l}$')
axi[0].set_ylabel(r'$\delta\phi_{wall,l}$',fontsize=24)
axi[1].plot(tline,maxval_r,'r',linewidth=2,label=r'$\delta\phi_{wall,r}$')
axi[1].set_ylabel(r'$\delta\phi_{wall,r}$',fontsize=24)
axi[2].plot(tline,massmin,'g',linewidth=2,label=r'$\partial_{\phi\phi}V_{min}$')
axi[2].set_ylabel(r'$\partial_{\phi\phi}V_{min}$',fontsize=24)
plt.xlabel(r'$t/T_{\mathrm{breather}}$',fontsize=30)
#plt.ylabel(r'$\delta\phi_{\mathrm{wall}}$',fontsize=30)
#plt.legend(fontsize=24)
plt.subplots_adjust(bottom=0.15,left=0.15,hspace=0)
plt.xlim(0.,1.5)
plt.savefig('field_wall_highstable.pdf')
plt.show()

plt.plot(tline,np_tot,'b',linewidth=2,label=r'$n_{eff}^{tot}$')
plt.plot(tline,np_left,'g--',linewidth=2,label=r'$n_{eff}^{left}$')
plt.plot(tline,np_right,'r-.',linewidth=2,label=r'$n_{eff}^{right}$')
plt.plot(tline,np_box,'c',linewidth=2,label=r'$n_{eff}^{(|x|<25)}$')
plt.xlabel(r'$t/T_{\mathrm{breather}}$',fontsize=30)
plt.ylabel(r'$n_{eff}$',fontsize=30)
plt.legend(fontsize=24,bbox_to_anchor=(0,0,1.1,1.1))
plt.subplots_adjust(bottom=0.15,left=0.13)
plt.savefig('npart_highstable.pdf')
plt.show()

plt.plot(xline,phi[0],linewidth=2,label=r'$t=15T_{breather}/32$')
plt.plot(xline,phi[128],linewidth=2,label=r'$t=T_{breather}/4$')
plt.plot(xline,phi[136],linewidth=2,label=r'$t=17T_{breather}/32$')
plt.plot(xline,phi[144],linewidth=2,label=r'$t=18T_{breather}/32$')
plt.plot(xline,phi[152],linewidth=2,label=r'$t=19T_{breather}/32$')
plt.xlim(-50,50)
plt.xlabel(r'$x$',fontsize=30)
plt.ylabel(r'$\phi$',fontsize=30)
plt.legend(fontsize=24,loc='upper left')
plt.yscale('symlog',linthreshy=0.05)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('field_bound_highstable.pdf')
plt.show()

clevs=np.linspace(-0.6,0.6,21)
#plt.contourf(x,t,phi[:,nlat/2-nkeep:nlat/2+nkeep],clevs,cmap=plt.cm.RdBu,extend='both')
plt.pcolor(x,t,phi[:,nlat/2-nkeep:nlat/2+nkeep],cmap=plt.cm.RdBu)
plt.xlim(-15,15)
plt.xlabel(r'$x$',fontsize=30)
plt.ylabel(r'$t/T_{\mathrm{breather}}$',fontsize=30)
cb=plt.colorbar(orientation='vertical')
cb.ax.set_ylabel(r'$\delta\phi$',fontsize=30)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('field_bound_highstable.png')
plt.show()
