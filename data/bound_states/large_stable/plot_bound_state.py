import numpy as np
import gc as gc  # garbage collector
import matplotlib as mpl

infile = "bound_state_v0.01_k0.83_a20_n4096.dat"
nlat=4096
nkeep=256
nbox=100  # keep +/- 25 around collision
k2=0.83
period=2.*np.pi*(1.+0.01**2)**0.5/0.01

# The file is huge, so don't do all the reads at one
a=np.genfromtxt(infile,usecols=[0])
x=np.reshape(a,(-1,nlat))
xline=np.array(x[0])
x=x[:,nlat/2-nkeep:nlat/2+nkeep]
a=[]
gc.collect()

a=np.genfromtxt(infile,usecols=[1])
t=np.reshape(a,(-1,nlat))
t=t/period
t=t[:,nlat/2-nkeep:nlat/2+nkeep]
tline=np.array(t[:,0])
a=[]
gc.collect()

a=np.genfromtxt(infile,usecols=[2])
phi=np.reshape(a,(-1,nlat))

a=np.genfromtxt(infile,usecols=[3])
dphi=np.reshape(a,(-1,nlat))

a=np.genfromtxt(infile,usecols=[6])
m2eff=np.reshape(a,(-1,nlat))

maxval_l=[]
maxval_r=[]
massmin=[]
for i in range(len(m2eff)):
    tmp=m2eff[i,:nlat/2]
    ind=np.argmin(tmp)
    if (abs(m2eff[i,ind]-m2eff[i,0])<1.e-5):
        ind=nlat/2-1
    maxval_l.append(phi[i,ind])
    tmp=m2eff[i,nlat/2:nlat]
    ind=np.argmin(tmp)
    if (abs(m2eff[i,ind+nlat/2]-m2eff[i,0])<1.e-5):
        ind=nlat/2-1
    maxval_r.append(phi[i,ind+nlat/2])
    massmin.append(m2eff[i,ind+nlat/2])

npart=k2*phi**2+dphi**2
np_left=[]
np_right=[]
np_tot=[]
np_box=[]
for i in range(len(npart)):
    np_tot.append(np.sum(npart[i]))
    np_box.append(np.sum(npart[i,nlat/2-nbox:nlat/2+nbox]))
    np_left.append(np.sum(npart[i][nlat/2-nbox:nlat/2]))
    np_right.append(np.sum(npart[i][nlat/2:nlat/2+nbox]))

import matplotlib.pyplot as plt

mpl.rc('xtick',labelsize=20)
mpl.rc('ytick',labelsize=20)

plt.plot(tline,maxval_l,linewidth=2,label=r'$\delta\phi_{left}$')
plt.plot(tline,maxval_r,linewidth=2,label=r'$\delta\phi_{right}$')
plt.plot(tline,massmin,linewidth=2,label=r'$\partial_{\phi\phi}V_{min}$')
plt.xlabel(r'$t/T_{\mathrm{breather}}$',fontsize=30)
plt.ylabel(r'$\delta\phi_{\mathrm{wall}}$',fontsize=30)
plt.legend(fontsize=24)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('field_wall_highstable.pdf')
plt.show()

plt.plot(tline,np_tot,linewidth=2,label=r'$n_{eff}^{tot}$')
plt.plot(tline,np_box,linewidth=2,label=r'$n_{eff}^{(|x|<25)}$')
plt.plot(tline,np_left,linewidth=2,label=r'$n_{eff}^{left}$')
plt.plot(tline,np_right,linewidth=2,label=r'$n_{eff}^{right}$')
plt.xlabel(r'$t/T_{\mathrm{breather}}$',fontsize=30)
plt.ylabel(r'$n_{eff}$',fontsize=30)
plt.legend(fontsize=24,bbox_to_anchor=(0,0,1.1,1.1))
plt.subplots_adjust(bottom=0.15,left=0.13)
plt.savefig('npart_highstable.pdf')
plt.show()

import matplotlib.colors as colors
import matplotlib.cm as cmx

t_plots=[64,80,96,112,128,144,160,176,192]
mycmap=plt.get_cmap('brg')
cNorm=colors.Normalize(vmin=0,vmax=len(t_plots)-1)
scalarMap=cmx.ScalarMappable(norm=cNorm,cmap=mycmap)

for i in range(len(t_plots)):
    tcur = t_plots[i]
    curcol=scalarMap.to_rgba(i)
    plt.plot(x[0,:],npart[tcur,nlat/2-nkeep:nlat/2+nkeep],linewidth=1.5,color=curcol,label=r'$t={:.2f}T_b$'.format(t[tcur,0]) )

plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.1,1.1),fontsize=24)
plt.xlim(-30,50)
plt.xlabel(r'$mx$',fontsize=30)
plt.ylabel(r'$\delta\dot{\phi}^2 + k_\perp^2\delta\phi^2$',fontsize=30)
#plt.yscale('symlog',linthreshy=0.05)
plt.subplots_adjust(bottom=0.16,left=0.17)
plt.savefig('npart_dist_highstable.pdf')
plt.show()

#plt.plot(xline,phi[0],linewidth=2,label=r'$t=15T_{breather}/32$')
#plt.plot(xline,phi[128],linewidth=2,label=r'$t=T_{breather}/2$')
#plt.plot(xline,phi[136],linewidth=2,label=r'$t=17T_{breather}/32$')
#plt.plot(xline,phi[144],linewidth=2,label=r'$t=18T_{breather}/32$')
#plt.plot(xline,phi[152],linewidth=2,label=r'$t=19T_{breather}/32$')
#plt.xlim(-50,50)
#plt.xlabel(r'$x$',fontsize=30)
#plt.ylabel(r'$\phi$',fontsize=30)
#plt.legend(fontsize=24,loc='upper left')
#plt.yscale('symlog',linthreshy=0.05)
#plt.subplots_adjust(bottom=0.15,left=0.15)
#plt.savefig('field_bound_highstable.pdf')
#plt.show()

clevs=np.linspace(-0.6,0.6,21)
plt.contourf(x,t,phi,clevs,cmap=plt.cm.RdBu,extend='both')
plt.xlim(-15,15)
plt.xlabel(r'$x$',fontsize=16)
plt.ylabel(r'$t/T_{\mathrm{breather}}$',fontsize=16)
cb=plt.colorbar(orientation='vertical')
cb.ax.set_ylabel(r'$\phi$',fontsize=16)
plt.show()
