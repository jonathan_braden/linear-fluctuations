#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np
from matplotlib.ticker import MaxNLocator

def makeplot(datfile,stamp,outfile,nlat):
    a=np.genfromtxt(datfile,usecols=[0,1,2])
    xvals=np.reshape(a[:,0],(-1,nlat))
    tvals=np.reshape(a[:,1],(-1,nlat))
    fvals=np.reshape(a[:,2],(-1,nlat))

    eps=0.05
    clevs=np.arange(-1.175,1.175+eps,eps)

    c=plt.contourf(tvals,xvals,fvals,clevs,cmap=plt.cm.RdYlBu,extend='both')
    myplt.insert_rasterized_contour_plot(c)
#    plt.pcolormesh(tvals,xvals,fvals,cmap=plt.cm.RdYlBu,vmin=-1.1,vmax=1.1,rasterized=True)
    cb=plt.colorbar(extend='both',orientation='vertical',ticks=[-1,0,1],pad=0.02)
    cb.solids.set_rasterized(True)
    cb.ax.set_ylabel(r'$\phi/\phi_0$',rotation='vertical',labelpad=0.5)
# hack to set tick sizes on colorbar
#    for tk in cb.ax.get_yticklabels():
#        tk.set_fontsize(18)

    plt.xlim(-60.,60.)
    plt.ylim(0.,80.)
    plt.ylabel(r'$mt$')
    plt.xlabel(r'$mx$')
    plt.text(190,14,stamp,horizontalalignment='right',verticalalignment='top', bbox=dict(facecolor='white',alpha=1))
    plt.gca().yaxis.set_major_locator(MaxNLocator(5))
    plt.gca().xaxis.set_major_locator(MaxNLocator(5))
#    plt.tight_layout(pad=0.5); #myplt.fix_axes_aspect( plt.gcf(), plt.gca() )
    plt.savefig(outfile)
    if showPreview:
        plt.show()
    plt.clf()

basedir='../data/bg_collisions/'
makeplot(basedir+'field_bubble_del0.1.dat',r'$\delta=1/10$','bubble_1d_del0.1.pdf',1024)
