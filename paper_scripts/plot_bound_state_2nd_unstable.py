import numpy as np

basedir='../data/bound_states/2nd_unstable/'
infile = basedir+"bound_state_k0.0006_10period_a20_n512.dat"
nlat=512
k2=0.0006
period=2.*np.pi*(1.+0.01**2)**0.5/0.01

a=np.genfromtxt(infile,usecols=[0,1,2,3,6])

x=a[:,0]
x=np.reshape(x,(-1,nlat))
t=a[:,1]
t=np.reshape(t,(-1,nlat))
t=t/period
phi=a[:,2]
phi=np.reshape(phi,(-1,nlat))
dphi=a[:,3]
dphi=np.reshape(dphi,(-1,nlat))
m2eff=a[:,4]
m2eff=np.reshape(m2eff,(-1,nlat))

maxval_l=[]
maxval_r=[]
massmin=[]
for i in range(len(m2eff)):
    tmp=m2eff[i,:nlat/2]
    ind_l=np.argmin(tmp)
    if (abs(m2eff[i,ind_l]-m2eff[i,0])<1.e-5):
        ind_l=nlat/2-1
    tmp=m2eff[i,nlat/2:]
    ind_r=np.argmin(tmp)+nlat/2
    if (abs(m2eff[i,ind_r]-m2eff[i,0])<1.e-5):
        ind_r=nlat/2
    maxval_l.append(phi[i,ind_l])
    maxval_r.append(phi[i,ind_r])
    massmin.append(m2eff[i,ind_l])

npart=k2*phi**2+dphi**2
np_left=[]
np_right=[]
np_tot=[]
np_box=[]
for i in range(len(npart)):
    np_tot.append(np.sum(npart[i]))
    np_left.append(np.sum(npart[i][:nlat/2-1]))
    np_right.append(np.sum(npart[i][nlat/2:]))
    np_box.append(np.sum(npart[i][nlat/2-100:nlat/2+100]))

np_tot=np.array(np_tot)
np_left=np.array(np_left)
np_right=np.array(np_right)

import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator, LogLocator
import matplotlib.gridspec as gspec

ax1=plt.subplot2grid((5,1),(0,0),rowspan=3)
ax3=plt.subplot2grid((5,1),(3,0),rowspan=2)
#leg_labels=[]
ax1.plot(t[:,0],maxval_l,'b',label=r'$\delta\phi_{l}$')
ax1.plot(t[:,0],maxval_r,'r-.',label=r'$\delta\phi_{r}$')
ax3.plot(t[:,0],massmin,'g')#,label=r"$\partial_{\phi\phi}V_{min}$")
ax3.set_xlabel(r'$t/T_{breather}$')
ax3.set_ylabel(r'$V_{\phi\phi}^{min}$',labelpad=-0.75)
ax1.set_ylabel(r'$\delta\phi_{wall}$',labelpad=-0.75)
#plt.legend(loc='upper left',bbox_to_anchor=(0,0,1,1),borderaxespad=0.01,ncol=2)
ax1.set_yscale('symlog',linthreshy=1.)
ax1.set_ylim(-1.e30,1.e30)
ax1.xaxis.set_ticklabels([])
ax3.set_yscale('linear')
ax3.set_yticks([-1,0,1])
ax3.set_ylim(-1.2,1.2)
ax1.set_yticks([-1.e20,0,1.e20])
#plt.subplots_adjust(hspace=0)
plt.savefig('field_wall_secondunstable.pdf')
plt.show()

plt.subplot(211)
plt.plot(t[:,0],np_tot/np_tot[0],label=r'$n_{eff}^{tot}$')
plt.xlabel(r'$t/T_{\mathrm{breather}}$')
plt.ylabel(r'$n_{eff}$',labelpad=0.25)
plt.yscale('log')
plt.gca().yaxis.set_major_locator(LogLocator(numticks=4))
plt.ylim(1.e-5,1.e50)
plt.gca().yaxis.set_ticks([1.,1.e25,1.e50])
plt.gca().xaxis.set_ticklabels([])
plt.subplot(212)
plt.plot(t[:,0],np_left/np_tot,'g',label=r'$n_{eff}^{left}$')
plt.plot(t[:,0],np_right/np_tot,'r-.',label=r'$n_{eff}^{right}$')
plt.yscale('linear')
plt.ylim([-0.1,1.1])
plt.ylabel(r'$n_{l/r}/n_{tot}$')
plt.gca().set_yticks([0,0.5,1])
plt.gca().yaxis.set_ticklabels([r'$0$','',r'$1$'])
#plt.legend(loc='lower right')
plt.savefig('npart_secondunstable.pdf')
plt.show()

# Fix these up, the 1/4 label is correct
#plt.plot(x[0,:],phi[0,:],'g--',linewidth=1.5)
#plt.plot(x[0,:],phi[112,:],linewidth=1.5,label=r'$t=7T_{breather}/32$')
#plt.plot(x[0,:],phi[120,:],linewidth=1.5,label=r'$t=15T_{breather}/64$')
#plt.plot(x[0,:],phi[128,:],linewidth=1.5,label=r'$t=T_{breather}/4$')
#plt.plot(x[0,:],phi[136,:],linewidth=1.5,label=r'$t=17T_{breather}/64$')
#plt.plot(x[0,:],phi[142,:],linewidth=1.5,label=r'$t=9T_{breather}/32$')
#plt.plot(x[0,:],phi[152,:],linewidth=1.5,label=r'$t=19T_{breather}/32$')
#plt.plot(x[0,:],phi[160,:],linewidth=1.5,label=r'$t=20T_{breather}/32$')
#plt.plot(x[0,:],phi[200,:],'b--',linewidth=1.5)

import matplotlib.colors as colors
import matplotlib.cm as cmx

#t_plots=[64,80,96,112,128,144,160,176,192]
t_plots=[112,128,144,160]
mycmap=plt.get_cmap('brg')
cNorm=colors.Normalize(vmin=0,vmax=len(t_plots)-1)
scalarMap=cmx.ScalarMappable(norm=cNorm,cmap=mycmap)

for i in range(len(t_plots)):
    tcur = t_plots[i]
    curcol=scalarMap.to_rgba(i)
    plt.plot(x[0,:],npart[tcur,:],color=curcol,label=r'$t={:.2f}T_b$'.format(t[tcur,0]) )

plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.1,1.1))
plt.xlim(-30,50)
plt.xlabel(r'$mx$')
plt.ylabel(r'$\delta\dot{\phi}^2 + k_\perp^2\delta\phi^2$')
#plt.yscale('symlog',linthreshy=0.05)
#plt.yscale('log')
plt.savefig('npart_dist_secondunstable.pdf')
plt.show()

#plt.contourf(x,t,np.log10(npart))
#plt.show()
#clevs=np.linspace(-0.6,0.6,21)
#plt.contourf(x,t,phi,clevs,cmap=plt.cm.RdBu,extend='both',norm=mpl.colors.SymLogNorm(linthresh=0.1)
#plt.xlim(-15,15)
#plt.xlabel(r'$x$',fontsize=16)
#plt.ylabel(r'$t/T_{\mathrm{breather}}$',fontsize=16)
#cb=plt.colorbar(orientation='vertical')
#cb.ax.set_ylabel(r'$\phi$',fontsize=16)
#plt.show()
