#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np

infile = "../data/modefuncs/sg_v0.01_k0.05/efunc_sg_v0.01_k0.05_1024perperiod.dat"

vval=0.01
period=2.*np.pi*(1.+0.01**2)**0.5/0.01
xrnge=(-20,20)
nlat=256
nperiod=1024

a=np.genfromtxt(infile,usecols=[0])
x=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[1])
t=np.reshape(a,(-1,nlat))
t=t/period
a=np.genfromtxt(infile,usecols=[2])
fld=np.reshape(a,(-1,nlat))

# Keep only part of the output
xdiff=80
x=x[:nperiod,nlat/2-xdiff:nlat/2+xdiff]
t=t[:nperiod,nlat/2-xdiff:nlat/2+xdiff]
fld=fld[:nperiod,nlat/2-xdiff:nlat/2+xdiff]

import matplotlib as mpl
import matplotlib.pyplot as plt; import myplotutils as myplt
from mpl_toolkits.mplot3d import axes3d

fig=plt.figure()
ax=fig.gca(projection='3d')

surf=ax.plot_surface(x,t,fld,rstride=2,cstride=1,cmap=plt.cm.RdBu,alpha=0.7,linewidth=0.,shade=True,vmin=-1,vmax=1.,norm=mpl.colors.SymLogNorm(linthresh=0.01,vmin=-1.,vmax=1.),rasterized=True)
#wire=ax.plot_wireframe(x,t,fld,rstride=40,cstride=16,color='k',linewidth=0.25)

ax.view_init(elev=35,azim=-45)

#cb=plt.colorbar(surf,ticks=[-2,-1,-0.5,-0.1,0,0.1,0.5,1,2])
#cb.solids.set_rasterized(True)

ax.xaxis._axinfo['label']['space_factor'] = 2.5
ax.xaxis._axinfo['ticklabel']['space_factor'] = 0.5
ax.yaxis._axinfo['label']['space_factor'] = 3.
ax.yaxis._axinfo['ticklabel']['space_factor'] = 0.5
ax.zaxis._axinfo['label']['space_factor'] = 3.
ax.zaxis._axinfo['ticklabel']['space_factor'] = 1.
plt.subplots_adjust(left=0.1,right=0.85)

ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$t/T_b$')
ax.set_xticks([-10,0,10])
ax.set_yticks([0,0.25,0.5])
ax.set_zticks([-2,0,2])
ax.set_ylim(0.,0.5)
ax.set_zlabel(r'$\delta\phi$')
ax.set_autoscale_on(False)
#ax.set_zscale('symlog')

#plt.savefig('sg_v0.01_k0.05_modefunc3d.png')
plt.savefig('sg_v0.01_k0.05_modefunc3d.pdf')
if showPreview:
    plt.show()
plt.clf()
