#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np

basedir='../data/convergence_test/nonlinear_integrator/energy_files/'

infiles=["en_l1024_n2048_dt0.4.dat","en_l1024_n2048_dt0.2.dat", "en_l1024_n2048_dt0.1.dat","en_l1024_n2048_dt0.05.dat"]#,"en_l1024_n2048_dt0.025.dat"]#,"en_l1024_n2048_dt0.0125.dat"]
labels=[r'$dt=\frac{4}{5}dx$',r'$dt=\frac{2}{5}dx$',r'$dt=\frac{1}{5}dx$',r'$dt=\frac{1}{10}dx$',r'$dt=\frac{1}{20}dx$']

tvals=[]
evals=[]
pvals=[]
for fname in infiles:
    a=np.genfromtxt(basedir+fname,usecols=[0,1,5])
    etmp=a[:,1]
    print "initial energy is ",etmp[0]
    etmp=etmp-etmp[0]
    evals.append(etmp)
    tvals.append(a[:,0])
    pvals.append(a[:,2])

import matplotlib.pyplot as plt; import myplotutils as myplt
from matplotlib.ticker import MaxNLocator, LogLocator

for i in range(len(evals)):
    plt.plot(tvals[i],np.abs(evals[i]),label=labels[i])

plt.xlabel(r'$mt$')
plt.ylabel(r'$|\rho-\rho_{init}|$')
plt.yscale('log')
plt.xlim(0,400)
plt.ylim(1.e-19,1.e-8)
plt.gca().set_xticks([0,100,200,300,400])
plt.gca().yaxis.set_major_locator(LogLocator(numticks=6))
#plt.legend(bbox_to_anchor=(0,0,1.07,1.15),loc='upper right')
plt.legend(loc='lower center',bbox_to_anchor=(-0.25,0.98,1.2,0.3),ncol=2,mode='expand',borderaxespad=0.)
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect( plt.gcf(), plt.gca() )
plt.savefig('energy_conservation.pdf')
if showPreview:
    plt.show()
plt.clf()

for i in range(len(pvals)):
    plt.plot(tvals[i],np.abs(pvals[i])*1.e18,label=labels[i])

plt.xlabel(r'$mt$')
plt.ylabel(r'$\left|\langle\dot{\phi}\partial_x\phi\rangle\right|$ [x$10^{-18}$]')
plt.ylim(0.,5.5)
plt.xlim(0,400)
plt.gca().set_xticks([0,100,200,300,400])
#plt.legend(loc='upper center',bbox_to_anchor=(0,0,1,1.15))
plt.legend(loc='lower center',bbox_to_anchor=(-0.25,0.98,1.2,0.3),ncol=2,mode='expand',borderaxespad=0.)
#plt.tight_layout(pad=0.5);  myplt.fix_axes_aspect( plt.gcf(), plt.gca() )
plt.savefig('momentum_conservation.pdf')
if showPreview:
    plt.show()
plt.clf()
