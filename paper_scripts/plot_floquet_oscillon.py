#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt
import sys

def main():
    print sys.argv
    showPreview = (sys.argv[1] == 'True')
    print "preview plots is ", showPreview, " for ",sys.argv[0]

    basedir='../data/floquet_charts/oscillon/'
    infile=basedir+"floquet_oscillon_n64.dat"
    outfile="floquet_oscillon"
    numk=36

    vvals, kvals,muvals = generate_data(infile,numk)
    plot_contour(vvals,kvals,muvals,[0,0.2,0.4],31)
    plt.gca().set_yticks([0.,0.1])
    plt.savefig(outfile+'.pdf')
    if showPreview:
        plt.show()
    plt.clf()


    infile=basedir+"floquet_oscillon_order2_n64.dat"
    outfile="floquet_oscillon_order2"
    numk=201
    vvals, kvals, muvals = generate_data(infile,numk)
    plot_contour(vvals,kvals,muvals,[0,1,2],31)
    plt.savefig(outfile+'.pdf')
    if showPreview:
        plt.show()
    plt.clf()


def generate_data(infile,numk):
    a=np.genfromtxt(infile,usecols=[0,1,2])
    vvals=a[:,0]
    vvals=np.reshape(vvals,(-1,numk))
    vvals=vvals.T

    kvals=a[:,1]
    kvals=np.reshape(kvals,(-1,numk))
    kvals=kvals.T

    muvals = a[:,2]
    muvals = np.reshape(muvals,(-1,numk))
    muvals=muvals.T

    return vvals, kvals, muvals

def plot_contour(vvals,kvals,muvals,cticks,nlev=31):
    c=plt.contourf(vvals,kvals,muvals,nlev,cmap=plt.cm.OrRd)
    myplt.insert_rasterized_contour_plot(c)
    plt.gca().xaxis.tick_top()
    plt.gca().xaxis.set_label_position('top')
    cb=plt.colorbar(c,pad=0.02,aspect=20,ticks=cticks,orientation='horizontal')
    cb.ax.set_xlabel(r'$\mu_{max}T_{oscillon}$')
    cb.solids.set_rasterized(True)
    cb.ax.xaxis.set_label_position('bottom')
    plt.ylabel(r'$k_\perp^2$',labelpad=0)
    plt.xlabel(r'$\epsilon$',labelpad=0)
#    plt.tight_layout(pad=0.5); #myplt.fix_axes_aspect(plt.gcf(), plt.gca())

if __name__ == '__main__':
    main()
