#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt
import gc as gc

def read_data(ifile,cols,nlat,period,nkeep=-1):
    a = np.genfromtxt(ifile, usecols=cols)

    x=np.reshape(a[:,0],(-1,nlat))
    t=np.reshape(a[:,1],(-1,nlat))
    t=t/period
    phi=np.reshape(a[:,2],(-1,nlat))
    dphi=np.reshape(a[:,3],(-1,nlat))
    m2eff=np.reshape(a[:,4],(-1,nlat))

    if (nkeep > 0):
        x=x[:,nlat/2-nkeep:nlat/2+nkeep]
        t=t[:,nlat/2-nkeep:nlat/2+nkeep]
        phi=phi[:,nlat/2-nkeep:nlat/2+nkeep]
        dphi=dphi[:,nlat/2-nkeep:nlat/2+nkeep]
        m2eff=m2eff[:,nlat/2-nkeep:nlat/2+nkeep]

    return x,t,phi,dphi,m2eff

def read_data_gc(ifile,cols,nlat,period,nkeep):
    a = np.genfromtxt(ifile, usecols=[cols[0]])
    x=np.reshape(a,(-1,nlat))
    x=x[:,nlat/2-nkeep:nlat/2+nkeep]

    a = np.genfromtxt(ifile, usecols=[cols[1]])
    gc.collect()
    t=np.reshape(a,(-1,nlat))
    t=t/period
    t=t[:,nlat/2-nkeep:nlat/2+nkeep]

    a = np.genfromtxt(ifile, usecols=[cols[2]])
    gc.collect()
    phi=np.reshape(a,(-1,nlat))
    phi=phi[:,nlat/2-nkeep:nlat/2+nkeep]

    a = np.genfromtxt(ifile,usecols=[cols[3]])
    gc.collect()
    dphi=np.reshape(a,(-1,nlat))
    dphi=dphi[:,nlat/2-nkeep:nlat/2+nkeep]

    a = np.genfromtxt(ifile,usecols=[cols[4]])
    m2eff=np.reshape(a,(-1,nlat))
    gc.collect()
    m2eff=m2eff[:,nlat/2-nkeep:nlat/2+nkeep]

    return x,t,phi,dphi,m2eff

def compute_minvals(phi,m2eff,nlat):
    nlat=len(phi[0])
    max_l=[]
    max_r=[]
    mmin=[]
    for i in range(len(m2eff)):
        tmp=m2eff[i,:nlat/2]
        ind_l=np.argmin(tmp)
        if (abs(m2eff[i,ind_l]-m2eff[i,0])<1.e-5):
            ind_l=nlat/2-1
        tmp=m2eff[i,nlat/2:]
        ind_r=np.argmin(tmp)+nlat/2
        if (abs(m2eff[i,ind_r]-m2eff[i,0])<1.e-5):
            ind_r=nlat/2-1
        max_l.append(phi[i,ind_l])
        max_r.append(phi[i,ind_r])
        mmin.append(m2eff[i,ind_l])
    return np.array(max_l),np.array(max_r),np.array(mmin)

def compute_npart(phi,dphi,k2):
    nlat=len(phi[0])
    npart=k2*phi**2+dphi**2
    np_l=[]
    np_r=[]
    np_t=[]
    for i in range(len(npart)):
        np_t.append(np.sum(npart[i]))
        np_l.append(np.sum(npart[i][0:nlat/2-1]))
        np_r.append(np.sum(npart[i][nlat/2:nlat]))
    return np.array(np_t),np.array(np_l),np.array(np_r), npart

def plot_phiwall(t,maxval_l,maxval_r,massmin,xlim,ylim,ytics,sym=False,xlab=False,add_legend=False):
    ylabel_loc=-0.23

    ax1=plt.subplot2grid((4,1),(0,0),rowspan=3)
    ax3=plt.subplot2grid((4,1),(3,0),rowspan=1)

    ax1.plot(t[:,0],maxval_l,'b',label=r'$\delta\phi_{wall,l}$')
    ax1.plot(t[:,0],maxval_r,'r-.',label=r'$\delta\phi_{wall,r}$')
    ax3.plot(t[:,0],massmin,'g')#,label=r'$\partial_{\phi\phi}V{min}$')
    if xlab:
        ax3.set_xlabel(r'$t/T_{\mathrm{breather}}$')
    ax3.set_ylabel(r'$V_{\phi\phi}^{min}$')
    ax1.set_ylabel(r'$\delta\phi_{\mathrm{wall}}$')
    if add_legend:
        ax1.legend(loc='upper center',bbox_to_anchor=(0,0,1,1),bbox_transform=plt.gcf().transFigure,ncol=2)
    ax1.set_ylim(ylim)
    if sym:
        ax1.set_yscale('symlog',linthreshy=1)
    ax1.set_xlim(xlim)
    ax1.set_xticklabels([])
    ax1.set_yticks(ytics)
    ax3.set_yscale('linear')
    ax3.set_xlim(xlim)
    ax3.set_ylim(-1.2,1.2)
    ax3.set_yticks([-1,0,1])
    ax3.set_yticklabels(r'$-1$',r'$0$',r'$1$')

    ax1.yaxis.set_label_coords(ylabel_loc,0.5)
    ax3.yaxis.set_label_coords(ylabel_loc,0.5)
    return

def plot_phiwall_3panel(t,maxval_l,maxval_r,massmin,xlim,xtics,ylim,ytics,sym=False,xlab=False,add_legend=False):
    ylabel_loc=-0.23

    ax1=plt.subplot2grid((8,1),(0,0),rowspan=3)
    ax2=plt.subplot2grid((8,1),(3,0),rowspan=3)
    ax3=plt.subplot2grid((8,1),(6,0),rowspan=2)

    ax1.plot(t[:,0],maxval_l,'b',label=r'$\delta\phi_{wall,l}$')
    ax2.plot(t[:,0],maxval_r,'r-.',label=r'$\delta\phi_{wall,r}$')
    ax3.plot(t[:,0],massmin,'g')#,label=r'$\partial_{\phi\phi}V{min}$')
    if xlab:
        ax3.set_xlabel(r'$t/T_{\mathrm{breather}}$')
 
    ax3.set_ylabel(r'$V_{\phi\phi}^{min}$')
    ax1.set_ylabel(r'$\delta\phi_{wall}^{left}$')
    ax2.set_ylabel(r'$\delta\phi_{wall}^{right}$')
    if add_legend:
        ax1.legend(loc='upper center',bbox_to_anchor=(0,0,1,1),bbox_transform=plt.gcf().transFigure,ncol=2)
    ax1.set_ylim(ylim)
    ax2.set_ylim(ylim)
    if sym:
        ax1.set_yscale('symlog',linthreshy=1)
        ax2.set_yscale('symlog',linthreshy=1)
    ax1.set_xlim(xlim)
    ax1.set_xticklabels([])
    ax1.set_yticks(ytics)
    ax2.set_xlim(xlim)
    ax1.set_xticks(xtics)
    ax2.set_xticks(xtics)
    ax2.set_xticklabels([])
    ax2.set_yticks(ytics)
    ax3.set_yscale('linear')
    ax3.set_xlim(xlim)
    ax3.set_xticks(xtics)
    ax3.set_ylim(-1.2,1.2)
    ax3.set_yticks([-1,0,1])
    ax3.set_yticklabels(r'$-1$',r'$0$',r'$1$')

    ax1.yaxis.set_label_coords(ylabel_loc,0.5)
    ax2.yaxis.set_label_coords(ylabel_loc,0.5)
    ax3.yaxis.set_label_coords(ylabel_loc,0.5)
    return

def plot_npart(np_tot,np_left,np_right,t,ylims,ytics,xlab=False,set_log=True,add_legend=False):
    ylabel_loc=-0.23
    plt.subplot(211)
    plt.plot(t[:,0],np_tot/np_tot[0],label=r'$n_{eff}^{tot}$')
    plt.ylabel(r'$n_{eff}$')
    if set_log:
        plt.gca().set_yscale('log')
#plt.gca().yaxis.set_major_locator(LogLocator(numticks=4))
    plt.gca().set_yticks(ytics)
    plt.gca().set_ylim(ylims)
    plt.gca().set_xticklabels([])
    plt.gca().yaxis.set_label_coords(ylabel_loc,0.5)
    plt.subplot(212)
    plt.plot(t[:,0],np_left/np_tot,'g',label=r'$n_{eff}^{left}$')
    plt.plot(t[:,0],np_right/np_tot,'r',label=r'$n_{eff}^{right}$')
    plt.gca().set_yscale('linear')
    plt.gca().set_ylim(-0.1,1.1)
    plt.gca().set_yticks([0,0.5,1])
#    plt.gca().set_yticklabels([r'$0$',r'$0.5$',r'$1$'])
    plt.gca().set_ylabel(r'$n_{l/r}/n_{tot}$')
    if xlab:
        plt.xlabel(r'$t/T_{\mathrm{breather}}$')
    plt.gca().yaxis.set_label_coords(ylabel_loc,0.5)
    if add_legend:
        plt.legend(loc='upper center',bbox_to_anchor=(0,0,1.,1),bbox_transform=plt.gcf().transFigure,ncol=2)
    return

import matplotlib.colors as colors
import matplotlib.cm as cmx
def plot_field_dist(xvals,tvals,npart,t_plots,xlab=False,add_legend=False):
    ylabel_loc=-0.22
    mycmap=plt.get_cmap('brg')
    cNorm=colors.Normalize(vmin=0,vmax=len(t_plots)-1)
    scalarMap=cmx.ScalarMappable(norm=cNorm,cmap=mycmap)

    for i in range(len(t_plots)):
        tc=t_plots[i]
        curcol=scalarMap.to_rgba(i)
        plt.plot( xvals[0,:],npart[tc,:],color=curcol,label=r'$t={:.2f}T_b$'.format(tvals[tc,0]) )

    plt.xlim(-25,25)
    plt.gca().set_xticks([-20,-10,0,10,20])
    if xlab:
        plt.xlabel(r'$mx$')
    plt.ylabel(r'$\delta\dot{\phi}^2+k_\perp^2\delta\phi^2$')
    if add_legend:
        plt.legend(loc='upper center',bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,borderaxespad=0.2,ncol=2,columnspacing=0.75,handletextpad=0.5)
    plt.gca().yaxis.set_label_coords(ylabel_loc,0.5)
    return

def make_plots():
    basedir='../data/bound_states/2nd_unstable/'
    infile = basedir+"bound_state_k0.0006_10period_a20_n512.dat"
    nlat=512
    k2=0.0006
    period=2.*np.pi*(1.+0.01**2)**0.5/0.01

    x,t,phi,dphi,m2eff=read_data(infile,[0,1,2,3,6],nlat,period)
    left,right,mass=compute_minvals(phi,m2eff,nlat)
    npt,npl,npr,npart=compute_npart(phi,dphi,k2)
    plot_npart(npt,npl,npr,t,(1.e-5,1.e55),[1.,1.e25,1.e50])
    plt.savefig('npart_secondunstable.pdf')
    if showPreview:
        plt.show()
    plt.clf()

    plot_phiwall(t,left,right,mass,(0,5),(-1.e25,1.e25),[-1.e20,0.,1.e20],sym=True)
    plt.savefig('field_wall_secondunstable.pdf')
    if showPreview:
        plt.show()
    plt.clf()
    plot_field_dist(x,t,npart,[112,128,144,160])
    plt.savefig('npart_dist_secondunstable.pdf')
    if showPreview:
        plt.show()
    plt.clf()


    basedir='../data/bound_states/k0.05/'
    infile = basedir+"bound_state_k0.05_10period_a20_n1024.dat"
    nlat=1024
    k2=0.05
    period=2.*np.pi*(1.+0.01**2)**0.5/0.01
    
    x,t,phi,dphi,m2eff=read_data(infile,[0,1,2,3,6],nlat,period)
    left,right,mass=compute_minvals(phi,m2eff,nlat)
    npt,npl,npr,npart=compute_npart(phi,dphi,k2)
    plot_npart(npt,npl,npr,t,(1.e-2,1.e28),[1.,1.e13,1.e26])
    plt.savefig('npart_k0.05.pdf')
    if showPreview:
        plt.show()
    plt.clf()

    plot_phiwall(t,left,right,mass,(0,2),(-1.e10,1.e10),[-1.e8,0.,1.e8],sym=True,xlab=False)
    plt.savefig('field_wall_k0.05.pdf')
    if showPreview:
        plt.show()
    plt.clf()

    plot_field_dist(x,t,npart,[112,128,144,160])
    plt.savefig('npart_dist_k0.05.pdf')
    if showPreview:
        plt.show()
    plt.clf()

if __name__=='__main__':
    make_plots()
