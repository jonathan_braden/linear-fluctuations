#!/usr/bin/python

######
# A collection of useful tools for making publication quality papers
######

import matplotlib.pyplot as plt
import numpy as np

#
# Give the plotted axes the same aspect ratio as the figure
# Curfig is the current figure, curax is the current axis
#
def fix_axes_aspect(curfig, curax, fix_to_fig=False, rat=0.5*(np.sqrt(5.)-1.) ):
    ax_rat=curax.get_data_ratio()
    if fix_to_fig:
        fig_size = curfig.get_size_inches()
        fig_rat = fig_size[1]/fig_size[0]
    else:
        fig_rat=rat
#    plt.axes().set_aspect(fig_rat/ax_rat,'box')
    curax.set_aspect(fig_rat/ax_rat,adjustable='box')

def get_ax_size(curax):
    bbox = curax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    width, height = bbox.width, bbox.height
    width *= fig.dpi
    height *= fig.dpi

def fix_axes_aspect_new(curfig,curax):
    plt.gca().set_position([0.15,0.15,0.95-0.15,0.95-0.15])

from matplotlib.collections import Collection
from matplotlib.artist import allow_rasterization

class ListCollection(Collection):
    def __init__(self, collections, **kwargs):
        Collection.__init__(self, **kwargs)
        self.set_collections(collections)
    def set_collections(self, collections):
        self._collections = collections
    def get_collections(self):
        return self._collections
    @allow_rasterization
    def draw(self, renderer):
        for _c in self._collections:
            _c.draw(renderer)

def insert_rasterized_contour_plot(c):
    collections = c.collections
    for _c in collections:
        _c.remove()
    cc = ListCollection(collections, rasterized=True)
    ax = plt.gca()
    ax.add_artist(cc)
    return cc
