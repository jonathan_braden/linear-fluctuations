#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]


import numpy as np

infile = "../data/modefuncs/sg_vsqrt2_k0.2/eigenfunction_vsqrt2_n256_k0.2_10period.dat"

vval=1./(2.**0.5-1.)
period=np.pi*(1.+vval**2)**0.5/vval
xrnge=(-20,20)
nlat=256
nperiod=256

a=np.genfromtxt(infile,usecols=[0])
x=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[1])
t=np.reshape(a,(-1,nlat))
t=t/period
a=np.genfromtxt(infile,usecols=[2])
fld=np.reshape(a,(-1,nlat))

# Keep only part of the output
xdiff=24
x=x[:2*nperiod,nlat/2-xdiff:nlat/2+xdiff]
t=t[:2*nperiod,nlat/2-xdiff:nlat/2+xdiff]
fld=fld[:2*nperiod,nlat/2-xdiff:nlat/2+xdiff]

import matplotlib as mpl
import matplotlib.pyplot as plt; import myplotutils as myplt
from mpl_toolkits.mplot3d import axes3d

fig=plt.figure()
ax=fig.gca(projection='3d')

surf=ax.plot_surface(x,t,fld,rstride=1,cstride=1,cmap=plt.cm.RdBu,alpha=0.7,linewidth=0.,shade=True,vmin=-0.4,vmax=0.4,rasterized=True)#,norm=mpl.colors.SymLogNorm(linthresh=1.,vmin=-2.,vmax=2.))
mesh=ax.plot_wireframe(x,t,fld,rstride=32,cstride=8,linewidth=0.25,color='k')
ax.margins(0.) #what does this do?
#cb=plt.colorbar(surf,ticks=[-0.4,-0.2,0,0.2,0.4],shrink=0.8,aspect=20,pad=0.12)
#cb.solids.set_rasterized(True)

ax.view_init(elev=25,azim=-45)

ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$t/T_{breather}$')
ax.set_xticks([-10,0,10])
ax.set_yticks([0,1,2])
ax.set_zticks([-0.5,0,0.5])
#ax.set_xlim(-12.,12.)
ax.set_ylim(0.,2.)
ax.set_zlabel(r'$\delta\phi$')
#ax.set_autoscale_on(False)
#ax.set_zscale('symlog')

ax.xaxis._axinfo['label']['space_factor'] = 2.5
ax.xaxis._axinfo['ticklabel']['space_factor'] = 0.5
ax.yaxis._axinfo['label']['space_factor'] = 2.5
ax.yaxis._axinfo['ticklabel']['space_factor'] = 0.5
ax.zaxis._axinfo['label']['space_factor'] = 3.5
ax.zaxis._axinfo['ticklabel']['space_factor'] = 1.5
plt.subplots_adjust(left=0.1,right=0.85)

#plt.savefig('sg_vsqrt2_k0.2_modefunc3d.png')
plt.savefig('sg_vsqrt2_k0.2_modefunc3d.pdf')
if showPreview:
    plt.show()
plt.clf()
