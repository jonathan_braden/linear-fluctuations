#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np
from matplotlib.ticker import MaxNLocator

basepath='../data/floquet_charts/collective/'
files=["floquet_r2.5_w40.dat","floquet_r3_w40.dat","floquet_r3.5_w40.dat"]
labels=[r'$r_{max}=2.5\sqrt{2}$',r'$r_{max}=3\sqrt{2}$',r'$r_{max}=3.5\sqrt{2}$']
markers=['b.','g^','rv']

for i in range(len(files)):
    a=np.genfromtxt(basepath+files[i],usecols=[0,1,2])
    plt.plot(a[:,1]**0.5*a[:,0],a[:,2],markers[i],label=labels[i])

plt.xlabel(r'$k_\perp T_{walls}$')
plt.ylabel(r'$\mu_{max}T_{walls}$')
plt.xlim(0,20)
plt.ylim(0,23)
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,borderaxespad=0.1)
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('floquet_2walls_max.pdf')
if showPreview:
    plt.show()
plt.clf()

for i in range(len(files)):
    a=np.genfromtxt(basepath+files[i],usecols=[0,1,3])
    plt.plot(a[:,1]**0.5*a[:,0],a[:,2],markers[i][0],label=labels[i])

plt.xlabel(r'$k_\perp T_{walls}$')
plt.ylabel(r'$\mu_{max,2} T_{walls}$')
plt.xlim(0,20)
plt.ylim(0.,3.1)
plt.gca().set_yticks([0,1,2,3])
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,borderaxespad=0.1)
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('floquet_2walls_max2.pdf')
if showPreview:
    plt.show()
plt.clf()
