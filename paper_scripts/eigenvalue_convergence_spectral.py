#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt
from matplotlib.ticker import LogLocator

v=0.5
l=58

#infiles=["floq_v0.5_L58_a5_n128_symp6.dat","floq_v0.5_L58_a10_n128_symp6.dat","floq_v0.5_L58_a20_n128_symp6.dat"]

basedir='../data/convergence_test/spectral/'
infiles=["floq_v0.5_L58_a5_n64_symp6.dat","floq_v0.5_L58_a10_n64_symp6.dat","floq_v0.5_L58_a20_n64_symp6.dat","floq_v0.5_L58_a40_n64_symp6.dat","floq_v0.5_L58_a80_n64_symp6.dat"]
name=[r"$dt^{(p)}=\frac{dx}{5}$",r"$dt^{(p)}=\frac{dx}{10}$",r"$dt^{(p)}=\frac{dx}{20}$",r"$dt^{(p)}=\frac{dx}{40}$"]
figname='evalue_convergence_n64.pdf'

#basedir='../data/convergence_test/spectral/'
#infiles=["floq_v0.5_L58_a20_n32_symp6.dat","floq_v0.5_L58_a20_n64_symp6.dat","floq_v0.5_L58_a20_n128_symp6.dat","floq_v0.5_L58_a20_n256_symp6.dat","floq_v0.5_L58_a20_n512_symp6.dat"]
#name=[r"$N^{(p)}=32$",r"$N^{(p)}=64$",r"$N^{(p)}=128$",r"$N^{(p)}=256$"]
#figname='evalue_convergence_spectral.pdf'

if (len(name)+1 != len(infiles)):
    print "Error, incorrect number of labels for input files"

fields=[]
muvals=[]
for i in range(len(infiles)):
    fname=basedir+infiles[i]
    a=np.genfromtxt(fname,usecols=[1,2])
    kvals=a[:,0]
    muvals.append(a[:,1])

diffs=[]
for i in range(len(muvals)-1):
    diffs.append(muvals[i+1]-muvals[i])

kvals=kvals*(1.+v**2)/v**2
for i in range(len(diffs)):
    plt.plot(kvals,np.abs(diffs[i]), label=name[i])

plt.xlabel(r'$k_{\perp}^2(1+v^2)/v^2$')
plt.ylabel(r'$|\mu_{max}^{(p+1)}-\mu_{max}^{(p)}|$')
plt.legend(loc='lower center', bbox_to_anchor=(-0.25,0.98,1.2,0.3),borderaxespad=0.0,ncol=2,mode='expand')

plt.yscale('log')
plt.ylim(1.e-20,1.e-3)
plt.xlim((0.,12.5))
plt.gca().yaxis.set_major_locator(LogLocator(numticks=5))
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect( plt.gcf(), plt.gca() )
plt.savefig(figname)
if showPreview:
    plt.show()
plt.clf()

