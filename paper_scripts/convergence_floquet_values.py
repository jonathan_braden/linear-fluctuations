#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np
v=0.5
l=58

basedir='../data/convergence_test/spectral/'
infile=basedir+"floq_v0.5_L58_a20_n64_symp6.dat"

a=np.genfromtxt(infile,usecols=[1,2])
kvals=a[:,0]
kvals=kvals*(1.+v**2)/v**2
muvals=a[:,1]

import matplotlib.pyplot as plt; import myplotutils as myplt
from matplotlib.ticker import MaxNLocator

plt.plot(kvals,2.*muvals)
plt.ylabel(r"$\mu_{max}T_{breather}$")
plt.xlabel(r"$k_{\perp}^2(1+v^2)/v^2$")
plt.ylim(-0.1,6.)
plt.gca().yaxis.set_major_locator(MaxNLocator(4))
plt.xlim(0.,12.5)
#plt.tight_layout(pad=0.5);  myplt.fix_axes_aspect( plt.gcf(), plt.gca() )

plt.savefig('floquet_v0.5_n64_a20.pdf')
if showPreview:
    plt.show()
plt.clf()
