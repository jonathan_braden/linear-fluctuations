#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np

infile="../data/floquet_charts/shape_mode/floquet_internal_L72w_n256.dat"
fsize=101
numk=26
outfile="internal_chart_spectral"

a=np.genfromtxt(infile,usecols=[0,1,2])
vvals=a[:,0]
vvals=np.reshape(vvals,(-1,numk))
vvals=vvals.T

kvals=a[:,1]
kvals=np.reshape(kvals,(-1,numk))
kvals=kvals.T

muvals=a[:,2]
muvals=np.reshape(muvals,(-1,numk))
muvals=muvals.T

import matplotlib.pyplot as plt; import myplotutils as myplt
c=plt.contourf(vvals,kvals,muvals,31,cmap=plt.cm.OrRd)
myplt.insert_rasterized_contour_plot(c)
cb=plt.colorbar(orientation='vertical',pad=0.02)
cb.ax.set_ylabel(r'$\mu_{max}T_{shape}$')
cb.set_ticks([0,0.5,1,1.5,2])
cb.solids.set_rasterized(True)
plt.ylabel(r'$k_\perp^2$')
plt.xlabel(r'$A_{shape}$')
plt.ylim(0.,0.9)
plt.gca().set_yticks([0,0.3,0.6,0.9])
plt.gca().set_xticks([0,1,2])
fig=plt.gcf()
#fig.tight_layout(pad=0.5); #myplt.fix_axes_aspect(plt.gcf(),plt.gca())
fig.savefig(outfile+'.pdf')

if showPreview:
    plt.show()
