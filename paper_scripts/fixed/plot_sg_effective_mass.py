#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np

def m2eff(x,t,v):
    gamma = 1./(1.+v**2)**0.5
    f=np.cos(t)/v/np.cosh(gamma*x)
    f=np.cos(4.*np.arctan(f))
    return f

def breather(x,t,v):
    gamma = 1./(1.+v**2)**0.5
    f=4.*np.arctan(np.cos(t)/v/np.cosh(gamma*x))
    return f

def xkink(t,v):
    gamma = 1./(1.+v**2)**0.5
    f=-np.sign(np.cos(t))*np.arccosh(np.abs(np.cos(t))/v) / gamma
    return f

import matplotlib.pyplot as plt; import myplotutils as myplt

def plot_effective_mass(tick_loc):
    c=plt.contourf(xvals,tvals,mvals,15,cmap=plt.cm.PuBu_r)
    myplt.insert_rasterized_contour_plot(c)
    plt.xlim(-10,10)
    plt.ylim(0,4.*np.pi)
    plt.xlabel(r'$x$')
    plt.ylabel(r'$\gamma v t$')
    cbar=plt.colorbar(ticks=tick_loc,aspect=15,pad=0.02)
    cbar.solids.set_rasterized(True)
    cbar.ax.set_ylabel(r'$V_{\phi\phi}(\phi_{breather})$')

    plt.text(0.05,0.95,r'$v=$'+vtext, bbox=dict(facecolor='white',alpha=1),transform=plt.gca().transAxes,horizontalalignment='left',verticalalignment='top')
#    plt.tight_layout(pad=0.5); #myplt.fix_axes_aspect(plt.gcf(),plt.gca())

    if add_kink:
        xloc = xkink(tvals,vval)
        plt.plot(xloc,tvals,'r',linewidth=1.5)

    plt.savefig('effmass_sg_v'+vstr+'.pdf')
    if showPreview:
        plt.show()
    plt.clf()

def plot_field_evolution():
    cvals=np.linspace(-np.pi,np.pi,29)
    c=plt.contourf(xvals,tvals,fvals,cvals,cmap=plt.cm.RdYlBu_r,extend='both',rasterized=True)
    myplt.insert_rasterized_contour_plot(c)
    plt.xlim(-10,10)
    plt.ylim(0,4.*np.pi)
    plt.xlabel(r'$x$')
    plt.ylabel(r'$\gamma v t$')
    cbar=plt.colorbar(ticks=[-np.pi,-0.5*np.pi,0,0.5*np.pi,np.pi],format=r"%2.1f",aspect=15,pad=0.02)
    cbar.ax.set_ylabel(r'$\phi_{\mathrm{breather}}$')
    cbar.solids.set_rasterized(True)
    cbar.ax.set_yticklabels([r'$-\pi$',r'$-\frac{\pi}{2}$',r'$0$',r'$\frac{\pi}{2}$',r'$\pi$'])
#cbar.ax.get_yticklabels()[0].set_fontsize(24)
#cbar.ax.get_yticklabels()[1].set_fontsize(28)
#cbar.ax.get_yticklabels()[2].set_fontsize(24)
#cbar.ax.get_yticklabels()[3].set_fontsize(28)
#cbar.ax.get_yticklabels()[4].set_fontsize(24)

    plt.text(0.05,0.95,r'$v=$'+vtext,bbox=dict(facecolor='white',alpha=1),transform=plt.gca().transAxes,horizontalalignment='left',verticalalignment='top')
#    plt.tight_layout(pad=0.5); #myplt.fix_axes_aspect(plt.gcf(),plt.gca())

    plt.savefig('breather_v'+vstr+'.pdf')
    if showPreview:
        plt.show()
    plt.clf()

xvals=np.linspace(-10.,10.,101)
tvals=np.linspace(0,4.*np.pi,101)

def get_mass_and_field(vcur):
    msq=[]; fld=[]
    for i in range(len(tvals)):
        msq.append(m2eff(xvals,tvals[i],vcur))
        fld.append(breather(xvals,tvals[i],vcur))
    return msq, fld

vval=1.
vtext=r'$1$'
vstr='1'
ticks_effmass=[-1.,0,1]
add_kink=False
mvals, fvals = get_mass_and_field(vval)
plot_effective_mass(ticks_effmass)
plot_field_evolution()

vval=1./(2.**0.5-1.)
vtext=r'$\frac{1}{\sqrt{2}-1}$'
vstr='sqrt2'
ticks_effmass=[0.,0.5,1.]
add_kink=False
mvals, fvals = get_mass_and_field(vval)
plot_effective_mass(ticks_effmass)
plot_field_evolution()

vval=0.01
vtext=r'$0.01$'
vstr='0.01'
ticks_effmass=[-1,0.,1.]
add_kink=True
mvals, fvals = get_mass_and_field(vval)
plot_effective_mass(ticks_effmass)
plot_field_evolution()
