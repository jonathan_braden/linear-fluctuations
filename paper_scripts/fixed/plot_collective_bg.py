#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np
from scipy.optimize import fsolve
from scipy.integrate import quad

basepath='../data/collective/'
files = ["collective_bg_r2.5.dat","collective_bg_r3.dat","collective_bg_r3.5.dat","collective_bg_r4.dat"]
labels = [r'$\omega r=2.5$',r'$\omega r=3$',r'$\omega r=3.5$',r'$\omega r=4$']

def analytic_approx(t,r1,r2):
    r=r1 + (1./2.**1.5)*np.log(np.cos(np.pi*t)**2+np.exp(-2.**1.5*(r1-r2)))
    return r

def potential(r):
    wr = 2.**(-0.5)*r
    f=-4. + 8.*r + 12./np.tanh(2.*r) - (24.*r+8.)/np.tanh(2.*r)**2 + 16.*r/np.tanh(2.*r)**3
    return 2.**0.5*f

rvals=[2.5,3.,3.5,4.]
periods = []
rmin = []
energies=[]
for rmax in rvals:
    rmaxc = rmax
    vmax = potential(rmaxc)
    V_norm = lambda x : vmax - potential(x)
    rminc = fsolve(V_norm, -0.5)[0]
    rmin.append(rminc)
# Make sure roundoff won't take us into a region of negative potenial (to do, mae this a while loop)
    while (V_norm(rminc) < 1.e-14):
        rminc = rminc+1.e-15
    while (V_norm(rmaxc) < 1.e-14):
        rmaxc = rmaxc - 1.e-15
    print V_norm(rminc), V_norm(rmax)
    V_norm = lambda x : (vmax - potential(x))**(-0.5)
    periods.append(quad(V_norm,rminc,rmax)[0])
    energies.append(vmax)

# The period is sqrt(2M)*(integral above) and M=4sqrt(2)/3
# The 2.**0.5 in front is since I expressed my potential in terms of omega r = r/2.**0.5, so the variable change introduces this factor which appears as a multiply when getting the period
periods = 2.**0.5*(8.*2.**0.5/3.)**0.5*np.array(periods)

import matplotlib.pyplot as plt; import myplotutils as myplt

plt.plot(1./np.sqrt(np.abs(energies)),periods)
plt.xlabel(r'$V_{max}^{-1/2}$')
plt.ylabel(r'$Period (T)$')
if showPreview:
    plt.show()
plt.clf()

# In this graph check if r is omega r or r
lc=['b','r','g','k']
step=[20,80,160,320]
tvals=np.linspace(0,2,201)
for i in range(len(rmin)):
    rmaxc=rvals[i]
    rminc=rmin[i]
    rtime=analytic_approx(tvals,2.**0.5*rmaxc,2.**0.5*rminc)
    plt.plot(tvals,rtime,lc[i],label=r'$\omega r_{max}='+str(rmaxc)+'$')
    print "rmax and rmin are ",rmaxc, rminc

# Now read in the data files
for i in range(len(files)):
    a=np.genfromtxt(basepath+files[i],usecols=[0,1])
    plt.plot(a[::step[i],0],a[::step[i],1],lc[i]+'^', alpha=0.7)

plt.ylabel(r'$r(t)$')
plt.xlabel(r'$t/T_{bg}$')
plt.xlim(0.,2.)
plt.legend(loc='lower center')
fig=plt.gcf()
#fig.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
fig.savefig('collective_backgrounds.pdf')
if showPreview:
    plt.show()
