#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt
import matplotlib as mpl

#
# Define the potential parameters in here
#
vvals=(0.01, 1., 1./(2.**0.5-1.))
print len(vvals)
vlabels=(r'$0.01$',r'$1$',r'$\frac{1}{\sqrt{2}-1}$')

def breather(x, t, v):
    gamma = 1./(1+v**2)**0.5
    return 4.*np.arctan(np.cos(gamma*v*t) / (v*np.cosh(gamma*x)))

def eff_mass(x,t,v):
    return np.cos(breather(x,t,v))

#
# Now make the plot
#
plt.xlabel(r'$x$')
plt.ylabel(r'$\phi$')

xvals = np.linspace(-10.,10.,101)
for i in range(len(vvals)):
    yvals=breather(xvals, 0., vvals[i])
    plt.plot(xvals, yvals, label=r'$v=$'+vlabels[i])

#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
#plt.legend(frameon=False)
plt.legend(bbox_to_anchor=(0,0,1.07,1.1))

fig=plt.gcf()
fig.savefig('breathers.pdf')
if showPreview:
    plt.show()
plt.clf()

plt.xlabel(r'$x$')
plt.ylabel(r'$\partial_{\phi\phi}V(\phi_{breather})$')

plt.gca().set_yticks([-1,0,1])
xvals = np.linspace(-10., 10., 101)
for i in range(len(vvals)):
    yvals=eff_mass(xvals, 0., vvals[i])
    plt.plot(xvals, yvals, label=r'$v=$'+vlabels[i])

#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.ylim(-1.2,1.2)
plt.legend(bbox_to_anchor=(0.,-0.04,1.07,1.),loc='lower right')

fig=plt.gcf()
fig.savefig('effmass_breathers.pdf')
if showPreview:
    plt.show()
plt.clf()
