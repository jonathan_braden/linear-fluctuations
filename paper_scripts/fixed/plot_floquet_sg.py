#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np

#infile="floquet_sg_n64_rat1e-5.dat"
#infile="floquet_smallv_n64_sg.dat"
infile="../data/floquet_charts/sinegordon/floquet_sinegordon_spectral_n64.dat"
numk=201

a=np.genfromtxt(infile,usecols=[0,1,2])
vvals=a[:,0]
vvals=np.reshape(vvals,(-1,numk))
vvals=vvals.T

kvals=a[:,1]
kvals=np.reshape(kvals,(-1,numk))
kvals=kvals.T

muvals=a[:,2]
muvals=np.reshape(muvals,(-1,numk))
muvals=muvals.T

# Redefine variables into appropriate coordinates
kvals=kvals*(1.+vvals**2)/vvals**2

import matplotlib.pyplot as plt; import myplotutils as myplt
c=plt.contourf(1./vvals,kvals,2.*muvals,31,cmap=plt.cm.OrRd)
myplt.insert_rasterized_contour_plot(c)
cb=plt.colorbar(orientation='vertical',pad=0.02)
cb.ax.set_ylabel(r'$\mu_{max}T_{breather}$')
cb.solids.set_rasterized(True)
cb.set_ticks([0,3,6,9])
plt.ylabel(r'$k_\perp^2(1+v^{-2})$')
plt.xlabel(r'$v^{-1}$')
#plt.tight_layout(pad=0.5); #myplt.fix_axes_aspect(plt.gcf(),plt.gca())

fig=plt.gcf()
fig.savefig('sg_chart.pdf')
if showPreview:
    plt.show()

