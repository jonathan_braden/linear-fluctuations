#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np

infile="../data/modefuncs/sg_v0.01_k0.05/eigenfunction_L538_n1024.dat"
nlat=1024

a=np.genfromtxt(infile,usecols=[0,1,2])
x=a[:,0]
x=x-x[nlat/2-1]
phi=a[:,1]
dphi=a[:,2]

import matplotlib.pyplot as plt; import myplotutils as myplt

plt.plot(x,phi,'b',label=r'$\delta\phi_{k_\perp}$')
plt.plot(x,dphi,'g',label=r'$\delta\dot{\phi}_{k_\perp}$')
plt.xlabel(r'$x$')
plt.ylabel(r'$\delta\phi_{k_\perp},\delta\dot{\phi}_{k_\perp}$',labelpad=-0.8)
plt.xlim(-100,100)
plt.ylim(-0.1,0.4)
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.05,1.1))
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())

fig=plt.gcf()
fig.savefig('sg_mode_initial_v0.01_k0.05.pdf')
if showPreview:
    plt.show()
plt.clf()
