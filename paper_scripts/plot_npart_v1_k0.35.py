#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import sys
sys.path.insert(0,'/home/jbraden/Documents/python_scripts/')

import myfileutils as myf
import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt
from matplotlib.ticker import LogLocator

infile="../data/modefuncs/sg_v1_k0.35/eigenfunction_v1_n512_k0.35_10period.dat"

vval = 1.
kperp=0.35**0.5

period = np.pi*(1.+vval**2)**0.5/vval  # period of effective mass, not bg
vals=myf.read_timeblock(infile,[0,1,2,3,4])
omega=np.pi/period

tvals=[]
fvals=[]
pvals=[]
npartf=[]
npartp=[]
for i in range(len(vals)):
    tvals.append(vals[i][1])
    fvals.append(vals[i][2])
    pvals.append(vals[i][3])

tvals=np.array(tvals)
tvals=tvals.T
tvals = tvals / 2. / period  # normalize to breather period
fvals=np.array(fvals)
pvals=np.array(pvals)
npartf=fvals**2
npartp=pvals**2

npartx=0.5*(npartp + omega**2*npartf) / omega

npart=[]
for i in range(len(npartx)):
    npart.append(np.sum(npartx[i]))

plt.plot(tvals[0],npart/npart[0])
plt.ylabel(r'$n_{\mathrm{eff}}^{\omega_{breather}}$',labelpad=0.5)
plt.xlabel(r'$t/T_{\mathrm{breather}}$')
plt.xlim(0,10)
plt.ylim(bottom=0.5)
plt.yscale('log')
#plt.gca().yaxis.set_major_locator(LogLocator(numticks=6))
plt.gca().set_yticks([1.e0,1.e5,1.e10,1.e15,1.e20])

#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())

fig=plt.gcf()
plt.savefig('npart_v1_k0.35.pdf')
if showPreview:
    plt.show()
plt.clf()
