#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np

#infile = "eigenfunction_internal_A0.5_k0.4_L125_2period_n256_spectral.dat"
#nlat=256

infile = "../data/modefuncs/internal_A0.5_k0.4/eigenfunction_internal_A0.5_k0.4_L125_2period_n512_spectral.dat"
nlat=512

period=2.*np.pi
floquet=0.73882793586629958/period
xrnge=(-20,20)

a=np.genfromtxt(infile,usecols=[0])
x=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[1])
t=np.reshape(a,(-1,nlat))
t=t
t2=t.T
a=np.genfromtxt(infile,usecols=[2])
f=np.reshape(a,(-1,nlat))
f2=f.T

a=np.genfromtxt(infile,usecols=[4])
m2eff=np.reshape(a,(-1,nlat))

fn=np.exp(-floquet*t)*f
fn2=fn.T
# x,t,f are now positions, times, and function values as a function of x in first column, versus time

# Now perform the FFTs
ntime=len(f)
ft=np.fft.rfft(fn,(ntime-1),axis=0)
ft2=ft.T

# Normalize the spectrum to the RMS at that location
flucpow=[]
fnorm=[]
fmax=[]
for i in range(len(ft2)):
    fpow=np.sum(abs(ft2[i])**2)
    flucpow.append(fpow)
    fnorm.append(ft2[i]/np.sqrt(fpow))
    fmax.append(np.max(abs(fnorm[i])))

fnorm=np.array(fnorm)
fnormt=fnorm.T
import matplotlib.pyplot as plt; import myplotutils as myplt
from matplotlib.ticker import MaxNLocator, LogLocator

c=plt.contourf(x,t/period,1.5*m2eff-1.,15,cmap=plt.cm.PuBu_r)
myplt.insert_rasterized_contour_plot(c)
plt.xlabel(r'$\sqrt{3/2}mx$')
plt.ylabel(r'$t/T_{shape}$')
plt.xlim(-15.,15.)
plt.gca().set_yticks([0,1,2])
plt.gca().xaxis.set_major_locator(MaxNLocator(6))
cb=plt.colorbar(pad=0.02)
cb.solids.set_rasterized(True)
cb.set_ticks([-1,0,1,2])
cb.set_label(r'$\partial_{\phi\phi}V$',labelpad=-0.8)
#plt.tight_layout(pad=0.5); #myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('internal_a0.5_m2eff.pdf')
if showPreview:
    plt.show()
plt.clf()

ax=plt.subplot(1,1,1)
ax.plot(x[0],np.abs(ft[1]),label=r'$\omega = \frac{\pi}{T_{shape}}$')
ax.plot(x[0],np.abs(ft[3]),label=r'$\omega = \frac{3\pi}{T_{shape}}$')
ax.plot(x[0],np.abs(ft[5]),label=r'$\omega = \frac{5\pi}{T_{shape}}$')
ax.set_yticks([])
plt.xlabel(r'$\sqrt{3/2}mx$')
plt.ylabel(r'$|\delta\tilde{\phi}_{\omega_i}(x)|$')
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.1,1.1))
plt.xlim(xrnge)
plt.gca().xaxis.set_major_locator(MaxNLocator(5))
#plt.ylim(0,80)
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('fourier_amp_internal_A0.5_k0.4.pdf')
if showPreview:
    plt.show()
plt.clf()

# Now make whatever plots I want
plt.plot(x[0],abs(fnormt[1]),label=r'$\omega = \frac{\pi}{T_{shape}}$')
plt.plot(x[0],abs(fnormt[3]),label=r'$\omega = \frac{3\pi}{T_{shape}}$')
plt.plot(x[0],abs(fnormt[5]),label=r'$\omega = \frac{5\pi}{T_{shape}}$')
plt.xlabel(r'$\sqrt{3/2}mx$')
plt.ylabel(r'$|\delta\tilde{\phi}_{\omega}(x)|/\sigma_{\omega}(x)$')
plt.xlim(xrnge)
plt.ylim(-0.1,1.1)
plt.gca().xaxis.set_major_locator(MaxNLocator(5))
plt.legend(loc='center right',bbox_to_anchor=(0,0,1.1,1))
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('fourier_power_internal_A0.5_k0.4.pdf')
if showPreview:
    plt.show()
plt.clf()

# Before plotting the angles, turn them into continuous variables
theta=np.zeros([3,len(ft2)])
theta[0,:]=np.angle(ft2[:,1])
theta[1,:]=np.angle(ft2[:,3])
theta[2,:]=np.angle(ft2[:,5])

# A better plan here would be to do some interpolations to determine the new x's
thetanew=[]
xnew=[]
for i in range(len(theta)):
    pos=np.where(np.abs(np.diff(theta[i])) > np.pi)[0]
    thetanew.append(np.insert(theta[i],pos+1,np.nan))
    xnew.append(np.insert(x[0],pos+1,np.nan))

plt.plot(xnew[0],thetanew[0],label=r'$\omega = \pi T_{\mathrm{breather}}^{-1}$')
plt.plot(xnew[1],thetanew[1],label=r'$\omega = 3\pi T_{\mathrm{breather}}^{-1}$')
plt.plot(xnew[2],thetanew[2],label=r'$\omega = 5\pi T_{\mathrm{breather}}^{-1}$')
plt.xlabel(r'$x$')
#plt.ylabel(r'$\mathrm{arg}(\delta\tilde{\phi}_{\omega})$',fontsize=16)
plt.ylabel(r'$\theta_{\omega_i}(x)$')
plt.ylim(-np.pi,np.pi)
plt.xlim(xrnge)
plt.legend(loc='center left')
if showPreview:
    plt.show()
plt.clf()

def continuous_angles(y):
    mid=len(y)/2-1
    wind=np.zeros(len(y))
    d=np.diff(y)
    winding_num=0
    for i in range(len(d)):
        if (d[i]>np.pi):
            winding_num-=1
        elif (d[i]<-np.pi):
            winding_num+=1
        wind[i+1]=winding_num

    wind=wind-wind[mid]
    return y+2.*np.pi*wind

thetanew=[]
xnew=[]
plt.plot(x[0],continuous_angles(theta[0])+4.*np.pi,label=r'$\omega = \frac{\pi}{T_{shape}}$')
plt.plot(x[0],continuous_angles(theta[1])+4.*np.pi,label=r'$\omega = \frac{3\pi}{T_{shape}}$')
plt.plot(x[0],continuous_angles(theta[2])+2.*np.pi,label=r'$\omega = \frac{5\pi}{T_{shape}}$')
plt.xlabel(r'$\sqrt{3/2}mx$')
#plt.ylabel(r'$\mathrm{arg}(\delta\tilde{\phi}_{\omega})$',fontsize=16)
plt.ylabel(r'$\theta_{i}(x)$',labelpad=-0.8)
plt.xlim(xrnge)
plt.ylim(-3*np.pi-0.5,5.*np.pi+0.5)
plt.gca().xaxis.set_major_locator(MaxNLocator(5))
plt.gca().set_yticks([-2.*np.pi,0.,2.*np.pi,4.*np.pi])
plt.gca().set_yticklabels([r'$-4\pi$',r'$-2\pi$',r'$0$',r'$2\pi$',r'$4\pi$'])
plt.legend(loc='lower right',bbox_to_anchor=(0,-0.03,1.1,1.))
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('fourier_arg_internal_A0.5_k0.4.pdf')
if showPreview:
    plt.show()
plt.clf()
