#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt

basedir='../data/floquet_charts/oscillon/'
infile=["floquet_oscillon_n64.dat","floquet_oscillon_order2_n64.dat"]#""
numk=[36,201]

vvals=[]
mu_vals=[]
for i in range(len(infile)):
    a=np.genfromtxt(basedir+infile[i],usecols=[0,1,2])
    v=np.reshape(a[:,0],(-1,numk[i]))
    mu=np.reshape(a[:,2],(-1,numk[i]))
    vvals.append(v[:,0])
    mu_vals.append(mu[:,0])

plt.plot(vvals[0],mu_vals[0],label=r'1$^{\mathrm{st}}$ Order')
plt.plot(vvals[1],mu_vals[1],label=r'2$^{\mathrm{nd}}$ Order')
#plt.legend(loc='upper left',ncol=2,mode='expand',bbox_to_anchor=(0,0,1,1))
plt.legend(loc='upper left',bbox_to_anchor=(0,0,1,1.25),borderaxespad=0.1)
plt.xlabel(r'$\epsilon$', labelpad=0)
plt.ylabel(r'$\mu_{max}T_{oscillon}$')
plt.ylim(-0.1,3.5)
plt.gca().set_yticks([0,1,2,3])
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(), plt.gca())
plt.savefig('floquet_oscillon_kperp0.pdf')
if showPreview:
    plt.show()
plt.clf()
