#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1]=='True')
print "preview plots is ", showPreview, " for ",sys.argv

from plot_bound_states import *
import numpy as np
import matplotlib.pyplot as plt

basedir='../data/bound_states/large_stable/'
infile=basedir+'bound_state_v0.01_k0.83_a20_n4096.dat'
nlat=4096
k2=0.83
period=2.*np.pi*(1.+0.01**2)**0.5/0.01
nkeep=256

x,t,phi,dphi,m2eff=read_data_gc(infile,[0,1,2,3,6],nlat,period,nkeep)
left,right,mass=compute_minvals(phi,m2eff,nlat)
npt,npl,npr,npart=compute_npart(phi,dphi,k2)
plot_npart(npt,npl,npr,t,(0.,10.),[1.,5.,10.],set_log=False,xlab=True,add_legend=False)
plt.savefig('npart_highstable.pdf')
if showPreview:
    plt.show()
plt.clf()

plot_phiwall_3panel(t,left,right,mass,(0,1.5),[0,0.5,1,1.5],(-2.5,2.5),[-1.5,0.,1.5],sym=False,xlab=True,add_legend=False)
plt.savefig('field_wall_highstable.pdf')
if showPreview:
    plt.show()
plt.clf()

plot_field_dist(x,t,npart,[112,128,144,160],add_legend=False,xlab=True)
plt.savefig('npart_dist_highstable.pdf')
if showPreview:
    plt.show()
plt.clf()
