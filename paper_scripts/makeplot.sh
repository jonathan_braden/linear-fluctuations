#!/bin/bash

#module load python/2.7.6
PREVIEW=False #True

function set_borders {
    echo 'figure.subplot.left   : '$1 >> matplotlibrc
    echo 'figure.subplot.bottom : '$2 >> matplotlibrc
    echo 'figure.subplot.right  : '$3 >> matplotlibrc
    echo 'figure.subplot.top    : '$4 >> matplotlibrc
}

sed 's/FIG_SIZE/figure.figsize : 1.98, 1.8/g' matplotlibrc-base > matplotlibrc
set_borders 0.27 0.12 0.95 0.74
echo 'figure.subplot.hspace  : 0.04' >> matplotlibrc
sed 's/legend.fontsize : 10.0/legend.fontsize : 9.0/g' matplotlibrc > tmp
mv -f tmp matplotlibrc
./plot_bound_states_1st_stable.py $PREVIEW
sed 's/FIG_SIZE/figure.figsize : 1.98, 1.4/g' matplotlibrc-base > matplotlibrc
set_borders 0.27 0.12 0.95 0.92
echo 'figure.subplot.hspace  : 0.04' >> matplotlibrc
./plot_bound_states.py $PREVIEW
sed 's/FIG_SIZE/figure.figsize : 1.98, 1.6/g' matplotlibrc-base > matplotlibrc
#sed 's/axes.labelsize  : 11.0/axes.labelsize : 10.0/g' matplotlibrc > tmp
#mv -f tmp matplotlibrc
echo 'figure.subplot.hspace : 0.06' >> matplotlibrc
set_borders 0.27 0.25 0.95 0.95
./plot_bound_states_high_stable.py $PREVIEW


sed 's/FIG_SIZE/figure.figsize : 1.98, 1.6/g' matplotlibrc-base > matplotlibrc
set_borders 0.23 0.24 0.95 0.8
./plot_oscillon_kperp0.py $PREVIEW
./plot_floquet_oscillon.py $PREVIEW

# Plots that are 0.48\linewidth
sed 's/FIG_SIZE/figure.figsize : 2.68, 1.80/g' matplotlibrc-base > matplotlibrc
set_borders 0.19 0.24 0.94 0.93
./plot_bg_collision.py $PREVIEW # could shift margins a bit left, or make smaller

sed 's/FIG_SIZE/figure.figsize : 2.68, 1.80/g' matplotlibrc-base > matplotlibrc
set_borders 0.20 0.22 0.95 0.91
./plot_efunc_winset_v0.01.py $PREVIEW # 12pt legend fine
./plot_npart_v1_k0.35.py $PREVIEW
./plot_npart_vsqrt2_k0.2.py $PREVIEW
./plot_npart_sgv0.01_k0.05.py $PREVIEW

sed 's/FIG_SIZE/figure.figsize : 2.68, 1.80/g' matplotlibrc-base > matplotlibrc
set_borders 0.17 0.22 0.92 0.91
./plot_sg_effective_mass.py $PREVIEW
./breather_plot.py $PREVIEW

sed 's/FIG_SIZE/figure.figsize : 2.68, 1.80/g' matplotlibrc-base > matplotlibrc
set_borders 0.19 0.22 0.94 0.91
./plot_collective_floquet.py $PREVIEW
./plot_floquet_sg.py $PREVIEW

#sed 's/FIG_SIZE/figure.figsize : 2.68, 1.80/g' matplotlibrc-base > matplotlibrc
#set_borders 0.15 0.22 0.9 0.91
#./plot_sg_effective_mass.py $PREVIEW

sed 's/FIG_SIZE/figure.figsize : 2.68, 1.80/g' matplotlibrc-base > matplotlibrc
set_borders 0.19 0.24 0.94 0.96
./plot_fft_internal_k0.4_amp0.5.py $PREVIEW
./plot_npart_internal_k0.4_amp0.5.py $PREVIEW

sed 's/FIG_SIZE/figure.figsize : 5.36, 3.60/g' matplotlibrc-base > matplotlibrc
echo "figure.subplot.wspace : 0.25" >> matplotlibrc
./plot_fft_v1_k0.35_multipanel.py $PREVIEW
./plot_fft_vsqrt2_k0.2_multipanel.py $PREVIEW

sed 's/FIG_SIZE/figure.figsize : 2.68, 2.1/g' matplotlibrc-base > matplotlibrc
set_borders 0.26 0.19 0.96 0.76
# odd-sized figures for purposes of fitting in legend
./field_convergence_varydx.py $PREVIEW
./field_convergence_varydt.py $PREVIEW
./energy_convergence.py $PREVIEW
./eigenvalue_convergence_spectral.py $PREVIEW
sed 's/FIG_SIZE/figure.figsize : 2.68, 2.1/g' matplotlibrc-base >matplotlibrc
set_borders 0.21 0.19 0.91 0.76
./convergence_floquet_values.py $PREVIEW # could shift this left

sed 's/FIG_SIZE/figure.figsize : 5.36, 1.8/g' matplotlibrc-base > matplotlibrc
set_borders 0.15 0.22 0.98 0.72
echo "figure.subplot.wspace : 0.03" >> matplotlibrc
./eigenvalue_convergence_multipanel.py $PREVIEW

# Plots that are 0.6\linewidth, todo: change tick padding to 4 or 5
sed 's/FIG_SIZE/figure.figsize  : 3.57, 2.4/g' matplotlibrc-base > matplotlibrc
set_borders 0.15 0.2 0.95 0.94
# small margins
./plot_collective_bg.py $PREVIEW  # use 10 pt legend
./collective_potential.py $PREVIEW
./plot_bg_bubble.py $PREVIEW
./pot_plot_linear.py $PREVIEW
./plot_internal_floquet.py $PREVIEW
# large margins

sed 's/FIG_SIZE/figure.figsize : 3.57, 2.4/g' matplotlibrc-base > matplotlibrc
./plot_efunc_3d_sg_v1_k0.35.py $PREVIEW
./plot_efunc_3d_sg_v0.01_k0.05.py $PREVIEW
./plot_efunc_3d_sg_vsqrt2_k0.2.py $PREVIEW

# Now subset fonts to reduce file size
#for f in *.pdf
#do
#    gs -dSAFER -dNOPAUSE -dBATCH -dQUIET -dSubsetFonts=true -sDEVICE=pdfwrite -sOutputFile=tmp.pdf $f
#    mv -f tmp.pdf $f
#done
# less effective: pdftocairo -pdf $f tmp.pdf