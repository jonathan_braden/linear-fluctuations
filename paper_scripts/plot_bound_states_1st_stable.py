#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv

from plot_bound_states import *
import numpy as np
import matplotlib.pyplot as plt

basedir='../data/bound_states/first_stable/'
infile=basedir+'bound_state_k4.006e-4_10period_a20_n512.dat'
nlat=512
k2=4.006e-4
period=2.*np.pi*(1.+0.01**2)**0.5/0.01

x,t,phi,dphi,m2eff=read_data(infile,[0,1,2,3,6],nlat,period)
left,right,mass=compute_minvals(phi,m2eff,nlat)
npt,npl,npr,npart=compute_npart(phi,dphi,k2)
plot_npart(npt,npl,npr,t,(1.e-1,1.e4),[1.,1.e2,1.e4],add_legend=True)
plt.savefig('npart_firststable.pdf')
if showPreview:
    plt.show()
plt.clf()

plot_phiwall(t,left,right,mass,(0,5),(-1.5,1.5),[-1.,0.,1.],sym=False,add_legend=True)
plt.savefig('field_wall_firststable.pdf')
if showPreview:
    plt.show()
plt.clf()

plot_field_dist(x,t,npart,[112,128,144,160],add_legend=True)
plt.savefig('npart_dist_firststable.pdf')
if showPreview:
    plt.show()
plt.clf()
