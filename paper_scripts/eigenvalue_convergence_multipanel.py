#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt
from matplotlib.ticker import LogLocator

def read_data(bdir,infiles,incols):
    kvals=[]
    muvals=[]
    for i in range(len(infiles)):
        fname=bdir+infiles[i]
        a=np.genfromtxt(fname,usecols=incols)
        kvals=a[:,0]
        muvals.append(a[:,1])
    diffs = []
    for i in range(len(muvals)-1):
        diffs.append(muvals[i+1]-muvals[i])
    return kvals, diffs

def make_plot(xvals,yvals,curax,noticks=True):
    leg_labels=[r'$N^{(p)}=32$',r'$N^{(p)}=64$',r'$N^{(p)}=128$',r'$N^{(p)}=256$']
    for i in range(len(yvals)):
        curax.plot(xvals,np.abs(yvals[i]),label=leg_labels[i])
    curax.set_yscale('log')
    curax.set_ylim(1.e-18,1.e2)
    curax.set_yticks([1.e-15, 1.e-10,1.e-5,1.])
    curax.set_xlim(0.,12.5)
    curax.set_xticks([0,4,8,12])
    if noticks:
        curax.set_yticklabels([])
    curax.set_xlabel(r'$k_\perp^2 (1+v^2)/v^2$',labelpad=0.75)
    return leg_labels

v=0.5
l=58

basedir='../data/convergence_test/spectral/'
infiles=["floq_v0.5_L58_a20_n32_symp6.dat","floq_v0.5_L58_a20_n64_symp6.dat","floq_v0.5_L58_a20_n128_symp6.dat","floq_v0.5_L58_a20_n256_symp6.dat","floq_v0.5_L58_a20_n512_symp6.dat"]
kvals, mudiff = read_data(basedir,infiles,[1,2])
kvals=kvals*(1.+v**2)/v**2
plt.subplot(131)
make_plot(kvals,mudiff,plt.gca(),noticks=False)
plt.gca().set_ylabel(r'$|\mu^{(p+1)}_{max}-\mu^{(p)}_{max}|$')
plt.gca().set_title(r'Spectral')

basedir='../data/convergence_test/discrete/'
infiles=["floq_v0.5_L58_a20_n32_symp6.dat","floq_v0.5_L58_a20_n64_symp6.dat","floq_v0.5_L58_a20_n128_symp6.dat","floq_v0.5_L58_a20_n256_symp6.dat","floq_v0.5_L58_a20_n512_symp6.dat"]
kvals, mudiff = read_data(basedir,infiles,[1,2])
kvals=kvals*(1.+v**2)/v**2
plt.subplot(132)
make_plot(kvals,mudiff,plt.gca())
plt.gca().set_title(r'Discrete $\mathcal{O}(dx^2)$')

basedir = '../data/convergence_test/discrete4/'
infiles=["floq_v0.5_L58_a20_n32_symp6.dat","floq_v0.5_L58_a20_n64_symp6.dat","floq_v0.5_L58_a20_n128_symp6.dat","floq_v0.5_L58_a20_n256_symp6.dat","floq_v0.5_L58_a20_n512_symp6.dat"]
kvals, mudiff = read_data(basedir,infiles,[1,2])
kvals=kvals*(1.+v**2)/v**2
plt.subplot(133)
make_plot(kvals,mudiff,plt.gca())
plt.gca().set_title(r'Discrete $\mathcal{O}(dx^4)$')

plt.legend(loc='lower center',ncol=4,bbox_to_anchor=(0.05,0.8,0.92,0.2), bbox_transform=plt.gcf().transFigure)

plt.savefig('floquet_convergence_varydx_multipanel.pdf')
if showPreview:
    plt.show()
plt.clf()
