for f in *.pdf
do
    gs -dSAFER -dNOPAUSE -dBATCH -dQUIET -dSubsetFonts=true -sDEVICE=pdfwrite -sOutputFile=tmp.pdf $f
    mv -f tmp.pdf $f
done