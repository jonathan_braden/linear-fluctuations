#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np

infile = "../data/modefuncs/sg_v1_k0.35/eigenfunction_v1_n512_k0.35_10period.dat"

period=4.4428829381583661
floquet=1.2699527643261310/period
xrnge=(-20,20)
nlat=512
nperiod=256

a=np.genfromtxt(infile,usecols=[0])
x=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[1])
t=np.reshape(a,(-1,nlat))
t=t/period
a=np.genfromtxt(infile,usecols=[2])
fld=np.reshape(a,(-1,nlat))

# Keep only part of the output
xdiff=32
x=x[:2*nperiod,nlat/2-xdiff:nlat/2+xdiff]
t=t[:2*nperiod,nlat/2-xdiff:nlat/2+xdiff]
fld=fld[:2*nperiod,nlat/2-xdiff:nlat/2+xdiff]

import matplotlib as mpl
import matplotlib.pyplot as plt; import myplotutils as myplt
from mpl_toolkits.mplot3d import axes3d

fig=plt.figure()
ax=fig.gca(projection='3d')

surf=ax.plot_surface(x,t,fld,rstride=2,cstride=2,cmap=plt.cm.RdBu,alpha=0.7,linewidth=0.,shade=True,rasterized=True,vmin=-1.,vmax=1.)#,vmin=-2,vmax=2.,norm=mpl.colors.SymLogNorm(linthresh=1.,vmin=-2.,vmax=2.))
mesh=ax.plot_wireframe(x,t,fld,rstride=64,cstride=8,linewidth=0.25,color='k')
ax.margins(0.)
#cb=plt.colorbar(surf,ticks=[-2,-1,-0.5,0,0.5,1,2],pad=0.12,shrink=0.8,aspect=20)
#cb.solids.set_rasterized(True)

ax.view_init(elev=25,azim=-45) # default is 45, -45

ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$t/T_b$')
ax.set_xticks([-4,0,4])
ax.set_yticks([0,1,2])
ax.set_zticks([-2,-1,0,1,2])
ax.set_ylim(0.,2.)
ax.set_zlabel(r'$\delta\phi$')
ax.set_autoscale_on(False)
#ax.set_zscale('symlog')

ax.xaxis._axinfo['label']['space_factor'] = 2.5
ax.xaxis._axinfo['ticklabel']['space_factor'] = 0.5
ax.yaxis._axinfo['label']['space_factor'] = 2.5
ax.yaxis._axinfo['ticklabel']['space_factor'] = 0.5
ax.zaxis._axinfo['label']['space_factor'] = 3.5
ax.zaxis._axinfo['ticklabel']['space_factor'] = 1.5
plt.subplots_adjust(left=0.1,right=0.85)

#plt.savefig('sg_v1_k0.35_modefunc3d.png')
plt.savefig('sg_v1_k0.35_modefunc3d.pdf')
if showPreview:
    plt.show()
plt.clf()
