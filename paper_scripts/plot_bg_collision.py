#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np

def makeplot(datfile,stamp,outfile,nlat):
    a=np.genfromtxt(datfile,usecols=[0,1,2])
    xvals=np.reshape(a[:,0],(-1,nlat))
    tvals=np.reshape(a[:,1],(-1,nlat))
    fvals=np.reshape(a[:,2],(-1,nlat))

    eps=0.1
    clevs=np.arange(-1.175,1.175+eps,eps)

#    plt.contourf(xvals,tvals,fvals,clevs,cmap=plt.cm.RdYlBu,extend='both')
    plt.pcolormesh(xvals,tvals,fvals,vmin=-1.175,vmax=1.175,cmap=plt.cm.RdYlBu,rasterized=True)

    cb=plt.colorbar(extend='both',orientation='vertical',ticks=[-1,0,1],pad=0.02)
    cb.solids.set_rasterized(True)
    cb.ax.set_ylabel(r'$\phi/\phi_0$',rotation='vertical',labelpad=-0.5)
# hack to set tick sizes on colorbar
#    for tk in cb.ax.get_yticklabels():
#        tk.set_fontsize(10)

    plt.xlim(0.,200.)
    plt.ylim(-16.,16.)
    plt.gca().set_yticks([-10,0,10])
    plt.xlabel(r'$mt$')
    plt.ylabel(r'$mx$',labelpad=0.25)
    plt.text(0.95,0.95,stamp, bbox=dict(facecolor='white',alpha=1),transform=plt.gca().transAxes,horizontalalignment='right',verticalalignment='top')
#    plt.tight_layout(pad=0.5); #myplt.fix_axes_aspect(plt.gcf(),plt.gca())
    plt.savefig(outfile)
    if showPreview:
        plt.show()
    plt.clf()

basedir='../data/bg_collisions/'
makeplot(basedir+'field_values_v0.05.dat',r'$u=0.05$','1d_collision_v0.05.pdf',256)
makeplot(basedir+'field_values_v0.2.dat',r'$u=0.2$','1d_collision_v0.2.pdf',256)
makeplot(basedir+'field_values_v0.3.dat',r'$u=0.3$','1d_collision_v0.3.pdf',256)
makeplot(basedir+'field_values_del1o30.dat',r'$\delta = 1/30$','1d_collision_v0_del1o30.pdf',512)
