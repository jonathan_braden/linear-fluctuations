#!/bin/bash

for f in *.pdf
do
    gs -dBATCH -dNOPAUSE -dSAFER -dQUIET -dNOPLATFONTS -dSubsetFonts=true -sDEVICE=pdfwrite -sOutputFile=tmp.pdf $f
    sleep 3
    oldsize=$(du -k $f | cut -f 1)
    newsize=$(du -k tmp.pdf | cut -f 1)
    echo "Old size is : "$oldsize" New size is : "$newsize

    if [ $newsize -le $oldsize ]; then
	echo "Moving file "$f
	mv tmp.pdf $f
    else
	echo "File "$f" retained"
    fi
done