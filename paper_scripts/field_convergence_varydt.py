#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np

basedir='../data/convergence_test/nonlinear_integrator/field_files/'

infiles=["field_l1024_n2048_dt0.4.dat","field_l1024_n2048_dt0.2.dat", "field_l1024_n2048_dt0.1.dat","field_l1024_n2048_dt0.05.dat","field_l1024_n2048_dt0.025.dat"]#,"field_l1024_n2048_dt0.0125.dat"]
latsize=[2048,2048,2048,2048,2048]#,2048]
labels=[r'$dt^{(p)}=\frac{4dx}{5}$',r'$dt^{(p)}=\frac{2dx}{5}$',r'$dt^{(p)}=\frac{dx}{5}$',r'$dt^{(p)}=\frac{dx}{10}$']#,r'$dt^{(p)}=dx/20$']
colors=['b','r','g','k','c']
nmin=1024

fields=[]
tvals=[]
for i in range(len(infiles)):
    fname=basedir+infiles[i]
    nsize=latsize[i]
    ds=nsize/nmin
    a=np.genfromtxt(fname,usecols=[2])
    ftmp=np.reshape(a,(-1,nsize))
    fields.append(ftmp[:,ds-1::ds])
    a=np.genfromtxt(fname,usecols=[0])
    tvals.append(a[0::latsize[0]])

# Adjust these comparisons as needed given the input list
diffs=[]
for i in range(len(fields)-1):
    diffs.append(fields[i+1]-fields[i])

norm=[]
for i in range(len(diffs)):
    ntmp=[]
    for j in range(len(diffs[i])):
        ntmp.append(np.sum(np.abs(diffs[i][j])))
    norm.append(ntmp)
norm=np.array(norm)/(nmin*1.)

nmax=[]
for i in range(len(diffs)):
    ntmp=[]
    for j in range(len(diffs[i])):
        ntmp.append(np.max(np.abs(diffs[i][j])))
    nmax.append(ntmp)
nmax=np.array(nmax)

import matplotlib.pyplot as plt; import myplotutils as myplt
from matplotlib.ticker import LogLocator, MaxNLocator

for i in range(len(nmax)):
    plt.plot(tvals[i],norm[i],colors[i]+'-',label=labels[i])
    plt.plot(tvals[i],nmax[i],colors[i]+'-.')

plt.yscale('log')
plt.xlim((0,400))
plt.ylim(1.e-16,1.e2)
plt.xlabel(r'$mt$')
plt.gca().yaxis.set_major_locator(LogLocator(numticks=5))
plt.gca().xaxis.set_major_locator(MaxNLocator(5))
plt.ylabel(r'$||\Delta\phi^{(p)}||_{L1,max}$')#,||\Delta\phi^{(p)}||_{max}$')
plt.legend(loc='lower center',bbox_to_anchor=(-0.25,0.98,1.2,0.3),ncol=2,mode='expand',borderaxespad=0.)
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect( plt.gcf(), plt.gca() )
plt.savefig('convergence_nonlinear_varyt.pdf')
if showPreview:
    plt.show()
plt.clf()
