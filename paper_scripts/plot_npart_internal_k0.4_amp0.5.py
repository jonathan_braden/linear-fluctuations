#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np

#infile="eigenfunction_internal_A0.5_k0.4_L125_2period_n512_spectral.dat"
#infile="eigenfunction_internal_A0.5_k0.4_L125_10period_n512_spectral.dat"
infile="../data/modefuncs/internal_A0.5_k0.4/eigenfunction_internal_A0.5_k0.4_L62_10period_n512_spectral.dat"
nlat=512
period=2.*np.pi
omega=np.pi/period

a=np.genfromtxt(infile,usecols=[0,1,2,3])
x=np.reshape(a[:,0],(-1,nlat))
t=np.reshape(a[:,1],(-1,nlat))
phi=np.reshape(a[:,2],(-1,nlat))
dphi=np.reshape(a[:,3],(-1,nlat))

npart=0.5/omega*(dphi**2+omega**2*phi**2)
ntot=[]
for i in range(len(npart)):
    ntot.append(np.sum(npart[i]))

import matplotlib.pyplot as plt; import myplotutils as myplt
from mpl_toolkits.mplot3d import axes3d
from matplotlib.ticker import LogLocator

plt.plot(t[:,0]/period,ntot/ntot[0])
plt.xlabel(r'$t/T_{shape}$')
plt.ylabel(r'$n_{eff}^{\omega_{shape}}$',labelpad=0.9)
plt.xlim(0,10)
plt.ylim(0.5,1.e7)
plt.yscale('log')
#plt.gca().yaxis.set_major_locator(LogLocator(numticks=4))
plt.gca().set_yticks([1.,1.e2,1.e4,1.e6])
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('internal_a0.5_k0.4_npart.pdf')
if showPreview:
    plt.show()
plt.clf()

xmin=nlat/2-128
xmax=nlat/2+128
fig=plt.figure()
ax=fig.gca(projection='3d')
surf=ax.plot_surface(x[:257,xmin:xmax],t[:257,xmin:xmax]/period,phi[:257,xmin:xmax],rstride=4,cstride=4,cmap=plt.cm.RdBu, alpha=0.7, linewidth=0.,vmin=-.5,vmax=.5,rasterized=True)
wire=ax.plot_wireframe(x[:257,xmin:xmax],t[:257,xmin:xmax]/period,phi[:257,xmin:xmax],rstride=32,cstride=32,color='k',linewidth=0.25)

ax.view_init(elev=35,azim=-45)
ax.xaxis._axinfo['label']['space_factor'] = 2.5
ax.xaxis._axinfo['ticklabel']['space_factor'] = 0.5
ax.yaxis._axinfo['label']['space_factor'] = 3.
ax.yaxis._axinfo['ticklabel']['space_factor'] = 0.5
ax.zaxis._axinfo['label']['space_factor'] = 3.
ax.zaxis._axinfo['ticklabel']['space_factor'] = 1.
#cb=plt.colorbar(surf,ticks=[-1,-0.5,0,0.5,1])
#cb.solids.set_rasterized(True)
ax.set_xlabel(r'$mx/\sqrt{3}$')
ax.set_ylabel(r'$t/T_{shape}$')
ax.set_zlabel(r'$\delta\phi$')
ax.set_xticks([-15,0,15])
ax.set_yticks([0,1,2])
ax.set_zticks([-0.5,0,0.5,1.])
ax.set_xlim(-20,20)
ax.set_ylim(0,2)
plt.subplots_adjust(left=0.05,right=0.85)
#plt.tight_layout(pad=0.5)
plt.savefig('internal_a0.5_k0.4_efunc.pdf')
if showPreview:
    plt.show()
plt.clf()

