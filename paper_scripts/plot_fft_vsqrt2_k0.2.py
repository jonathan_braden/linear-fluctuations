#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np

infile = "../data/modefuncs/sg_vsqrt2_k0.2/eigenfunction_vsqrt2_n256_k0.2_1period.dat"
period=3.4004353847414772  # for 2**0.5-1
floquet=0.36253624190137057 #for k^2=0.2
floquet=floquet/period
xrnge=(-35,35)
nlat=256
nxtick=6

a=np.genfromtxt(infile,usecols=[0])
x=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[1])
t=np.reshape(a,(-1,nlat))
t2=t.T
a=np.genfromtxt(infile,usecols=[2])
f=np.reshape(a,(-1,nlat))
f2=f.T

fn=np.exp(-floquet*t)*f
fn2=fn.T
# x,t,f are now positions, times, and function values as a function of x in first column, versus time

# Now perform the FFTs
ntime=len(f)
ft=np.fft.rfft(fn,(ntime-1),axis=0)
ft2=ft.T

# Normalize the spectrum to the RMS at that location
flucpow=[]
fnorm=[]
fmax=[]
for i in range(len(ft2)):
    fpow=np.sum(abs(ft2[i])**2)
    flucpow.append(fpow)
    fnorm.append(ft2[i]/np.sqrt(fpow))
    fmax.append(np.max(abs(fnorm[i])))

fnorm=np.array(fnorm)
fnormt=fnorm.T
import matplotlib.pyplot as plt; import myplotutils as myplt
from matplotlib.ticker import MaxNLocator

ax=plt.subplot(1,1,1)
ax.plot(x[0],np.abs(ft[1]),label=r'$\omega=2\pi T_{b}^{-1}$')
ax.plot(x[0],np.abs(ft[3]),label=r'$\omega=6\pi T_{b}^{-1}$')
ax.plot(x[0],np.abs(ft[5]),label=r'$\omega=10\pi T_{b}^{-1}$')
ax.set_yticks([])
ax.xaxis.set_major_locator(MaxNLocator(nxtick))
plt.xlabel(r'$x$')
plt.ylabel(r'$|\delta\tilde{\phi}_{\omega_i}(x)|$')
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.08,1.1))
plt.xlim(xrnge)
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('fourier_amp_vsqrt_k0.2.pdf')
if showPreview:
    plt.show()
plt.clf()

plt.plot(x[0],abs(fnormt[1]),label=r'$\omega = 2\pi T_{b}^{-1}$')
plt.plot(x[0],abs(fnormt[3]),label=r'$\omega = 6\pi T_{b}^{-1}$')
plt.plot(x[0],abs(fnormt[5]),label=r'$\omega = 10\pi T_{b}^{-1}$')
plt.xlabel(r'$x$')
plt.ylabel(r'$|\delta\tilde{\phi}_{\omega_i}(x)|/\sigma_\omega(x)$')
plt.xlim(xrnge)
plt.ylim(0,1.1)
ax.xaxis.set_major_locator(MaxNLocator(nxtick))
plt.legend(loc='center right',bbox_to_anchor=(0,0,1.08,1))
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('fourier_power_vsqrt2_k0.2.pdf')
if showPreview:
    plt.show()
plt.clf()

# Before plotting the angles, turn them into continuous variables
theta=np.zeros([3,len(ft2)])
theta[0,:]=np.angle(ft2[:,1])
theta[1,:]=np.angle(ft2[:,3])
theta[2,:]=np.angle(ft2[:,5])

# A better plan here would be to do some interpolations to determine the new x's
thetanew=[]
xnew=[]
for i in range(len(theta)):
    pos=np.where(np.abs(np.diff(theta[i])) > np.pi)[0]
    thetanew.append(np.insert(theta[i],pos+1,np.nan))
    xnew.append(np.insert(x[0],pos+1,np.nan))

plt.plot(xnew[0],thetanew[0],label=r'$\omega = 2\pi T_{\mathrm{breather}}^{-1}$',linewidth=2)
plt.plot(xnew[1],thetanew[1],label=r'$\omega = 6\pi T_{\mathrm{breather}}^{-1}$',linewidth=2)
plt.plot(xnew[2],thetanew[2],label=r'$\omega = 10\pi T_{\mathrm{breather}}^{-1}$',linewidth=2)
plt.xlabel(r'$x$',fontsize=16)
#plt.ylabel(r'$\mathrm{arg}(\delta\tilde{\phi}_{\omega})$',fontsize=16)
plt.ylabel(r'$\theta_{\omega_i}(x)$')
plt.ylim(-np.pi,np.pi)
plt.xlim(xrnge)
plt.legend(loc='center')
if showPreview:
    plt.show()
plt.clf()

def continuous_angles(y):
    mid=len(y)/2-1
    wind=np.zeros(len(y))
    d=np.diff(y)
    winding_num=0
    for i in range(len(d)):
        if (d[i]>np.pi):
            winding_num-=1
        elif (d[i]<-np.pi):
            winding_num+=1
        wind[i+1]=winding_num

    wind=wind-wind[mid]
    return y+2.*np.pi*wind

thetanew=[]
xnew=[]
plt.plot(x[0],continuous_angles(theta[0])+2.*np.pi,label=r'$\omega = 2\pi T_{b}^{-1}$')
plt.plot(x[0],continuous_angles(theta[1])+2.*np.pi,label=r'$\omega = 6\pi T_{b}^{-1}$')
plt.plot(x[0],continuous_angles(theta[2])+2.*np.pi,label=r'$\omega = 10\pi T_{b}^{-1}$')
plt.xlabel(r'$x$')
#plt.ylabel(r'$\mathrm{arg}(\delta\tilde{\phi}_{\omega})$',fontsize=16)
plt.ylabel(r'$\theta_{i}(x)$',labelpad=0)
plt.xlim(xrnge)
plt.ylim(-3*np.pi,3.*np.pi)
plt.gca().set_yticks([-2.*np.pi,0,2.*np.pi])
plt.gca().set_yticklabels([r'$-2\pi$',r'$0$',r'$2\pi$'])
ax.xaxis.set_major_locator(MaxNLocator(nxtick))
plt.legend(loc='lower right',bbox_to_anchor=(0,0,1.08,1))
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('fourier_arg_vsqrt2_k0.2.pdf')
if showPreview:
    plt.show()
plt.clf()
