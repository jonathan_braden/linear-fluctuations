#!/bin/bash

for fcur in *.py; do
#    sed -i 's/plt.tight_layout()/plt.tight_layout(); myplt.fix_axes_aspect(plt.gcf(),plt.gca())/g' $fcur
#    sed -i 's/fig.tight_layout()/fig.tight_layout(); myplt.fix_axes_aspect(plt.gcf(),plt.gca())/g' $fcur
    sed -i 's/fig.tight_layout()/fig.tight_layout(pad=0.3)/g' $fcur
    done