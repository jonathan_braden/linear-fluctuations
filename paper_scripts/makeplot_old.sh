#!/bin/bash

#module load python/2.7.6
PREVIEW=F   #True

# The actual plotting commands
#sed 's/FIG_SIZE/figure.figsize : 1.90, 1.18/g' matplotlibrc-base > matplotlibrc
sed 's/FIG_SIZE/figure.figsize : 2.68, 2/g' matplotlibrc-base > matplotlibrc
# use matplotlibrc-half for some of these ...
./eigenvalue_convergence_discrete2.py $PREVIEW
./eigenvalue_convergence_discrete4.py $PREVIEW
./eigenvalue_convergence_spectral.py $PREVIEW

./plot_collective_floquet.py $PREVIEW

./plot_floquet_oscillon.py $PREVIEW
./plot_oscillon_kperp0.py $PREVIEW

./plot_fft_vsqrt2_k0.2.py $PREVIEW  # fix
./plot_fft_v1_k0.35.py $PREVIEW

# Plots that are 0.45\linewidth
#sed 's/FIG_SIZE/figure.figsize : 2.68, 1.66/g' matplotlibrc-base > matplotlibrc # change figure dimensions in matplotlibrc
sed 's/FIG_SIZE/figure.figsize : 2.68, 1.80/g' matplotlibrc-base > matplotlibrc
# large margins
# side-by-side in text
./plot_efunc_winset_v0.01.py $PREVIEW
# shift over with same vertical bbox
./plot_sg_effective_mass.py $PREVIEW

./convergence_floquet_values.py $PREVIEW
./field_convergence_varydt.py $PREVIEW
./field_convergence_varydx.py $PREVIEW
./energy_convergence.py $PREVIEW

./breather_plot.py $PREVIEW
./plot_bg_collision.py $PREVIEW

./plot_collective_floquet.py $PREVIEW
./plot_floquet_sg.py $PREVIEW

./plot_fft_internal_k0.4_amp0.5.py $PREVIEW
./plot_efunc_winset_v0.01.py $PREVIEW
./plot_npart_sgv0.01_k0.05.py $PREVIEW
./plot_npart_internal_k0.4_amp0.5.py $PREVIEW
./plot_npart_v1_k0.35.py $PREVIEW
./plot_npart_vsqrt2_k0.2.py $PREVIEW


# Plots that are 0.6\linewidth
#sed 's/FIG_SIZE/figure.figsize : 3.57, 2.21/g' matplotlibrc-base > matplotlibrc
sed 's/FIG_SIZE/figure.figsize  : 3.57, 2.4/g' matplotlibrc-base > matplotlibrc
# small margins
./plot_collective_bg.py $PREVIEW
./collective_potential.py $PREVIEW
./plot_bg_bubble.py $PREVIEW
# large margins

./plot_efunc_3d_sg_v1_k0.35.py $PREVIEW
./plot_efunc_3d_sg_v0.01_k0.05.py $PREVIEW
./plot_efunc_3d_sg_vsqrt2_k0.2.py $PREVIEW
#./plot_efunc_3d_internal.py $PREVIEW

# Plots that are 0.75\linewidth
sed 's/FIG_SIZE/figure.figsize : 4.45, 2.76/g' matplotlibrc-base > matplotlibrc
./collective_potential.py $PREVIEW
./pot_plot_linear.py $PREVIEW
./plot_collective_bg.py $PREVIEW

./plot_bg_bubble.py $PREVIEW
./plot_internal_floquet.py $PREVIEW

# Now subset fonts to reduce file size
for f in *.pdf
do
    gs -dSAFER -dNOPAUSE -dBATCH -dQUIET -dSubsetFonts=true -sDEVICE=pdfwrite -sOutputFile=tmp.pdf $f
    mv -f tmp.pdf $f
done
# less effective: pdftocairo -pdf $f tmp.pdf