#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np

basedir='../data/convergence_test/nonlinear_integrator/field_files/'

#infiles=["field_l1024_n2048_dt0.1.dat","field_l1024_n4096_dt0.1.dat","field_l1024_n8192_dt0.1.dat"]
#infiles=["field_l1024_n2048_dt0.2.dat","field_l1024_n4096_dt0.1.dat","field_l1024_n8192_dt0.05.dat","field_l1024_n16384_dt0.025.dat"]
infiles=["field_l1024_n2048_dt0.1.dat","field_l1024_n4096_dt0.05.dat","field_l1024_n8192_dt0.025.dat","field_l1024_n16384_dt0.0125.dat"]
latsize=[2048,4096,8192,8192*2]
dxvals=['2','4','8','16']
colors=['b','r','g','k']

nmin=1024
deln=128

a=np.genfromtxt(basedir+infiles[0],usecols=[0])
tvals=a[0::latsize[0]]

fields=[]
for i in range(len(infiles)):
    fname=basedir+infiles[i]
    nsize=latsize[i]
    ds=nsize/nmin
    a=np.genfromtxt(fname,usecols=[2])
    ftmp=np.reshape(a,(-1,nsize))
    fields.append(ftmp[:,ds-1::ds])

a=[]
# Adjust these comparisons as needed given the input list
diffs=[]
for i in range(len(fields)-1):
    diffs.append(fields[i+1]-fields[i])

norm=[]
norm_b=[]
for i in range(len(diffs)):
    ntmp=[]
    ntmp1=[]
    for j in range(len(diffs[i])):
        ntmp.append(np.sum(np.abs(diffs[i][j])))
        ntmp1.append(np.sum(np.abs(diffs[i][j,nmin/2-deln:nmin/2+deln])))
    norm.append(ntmp)
    norm_b.append(ntmp1)
norm=np.array(norm)/(nmin*1.)
ltmp=len(diffs[0][0,nmin/2-deln:nmin/2+deln])
norm_b=np.array(norm_b)/(1.*ltmp)

nmax=[]
for i in range(len(diffs)):
    ntmp=[]
    for j in range(len(diffs[i])):
        ntmp.append(np.max(np.abs(diffs[i][j])))
    nmax.append(ntmp)
nmax=np.array(nmax)

import matplotlib.pyplot as plt; import myplotutils as myplt
from matplotlib.ticker import MaxNLocator, LogLocator

for i in range(len(norm)):
    plt.plot(tvals,norm[i],colors[i]+'-',label=r'$dx^{(p)}=\frac{1}{'+dxvals[i]+'m}$')
#    plt.plot(tvals,norm_b[i],colors[i]+'-.')
    plt.plot(tvals,nmax[i],colors[i]+'--')

plt.yscale('log')
plt.xlim(0,400)
plt.ylim(1.e-16,1.e2)
plt.gca().xaxis.set_major_locator(MaxNLocator(5))
plt.gca().yaxis.set_major_locator(LogLocator(numticks=5))
plt.xlabel(r'$mt$')
plt.ylabel(r'$||\Delta\phi^{(p)}||_{L1,max}$') #,||\Delta\phi^{(p)}||_{max}$')
plt.legend(loc='lower center',bbox_to_anchor=(-0.25,0.98,1.2,0.3),ncol=2,mode='expand',borderaxespad=0.)
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect( plt.gcf(), plt.gca() )
plt.savefig('convergence_nonlinear_varyn.pdf')
if showPreview:
    plt.show()
plt.clf()

# Now plot the difference between the solutions in the middle
tstart=600
plt.plot(tvals[tstart:],fields[0][tstart:,nmin/2-1])
plt.plot(tvals[tstart:],fields[1][tstart:,nmin/2-1])
plt.plot(tvals[tstart:],fields[2][tstart:,nmin/2-1])
plt.plot(tvals[tstart:],fields[3][tstart:,nmin/2-1])
