#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt
import matplotlib as mpl
from matplotlib.ticker import MaxNLocator

#
# Define the potential parameters in here
#
vvals=(0.01, 1., 1./(2.**0.5-1.))
print len(vvals)
vlabels=(r'$0.01$',r'$1$',r'$\frac{1}{\sqrt{2}-1}$')

def breather(x, t, v):
    gamma = 1./(1+v**2)**0.5
    return 4.*np.arctan(np.cos(gamma*v*t) / (v*np.cosh(gamma*x)))

def eff_mass(x,t,v):
    return np.cos(breather(x,t,v))

#
# Now make the plot
#
plt.xlabel(r'$x$')
plt.ylabel(r'$\phi$')

xvals = np.linspace(-10.,10.,101)
for i in range(len(vvals)):
    yvals=breather(xvals, 0., vvals[i])
    plt.plot(xvals, yvals, label=r'$v=$'+vlabels[i])

#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
#plt.legend(frameon=False)
plt.ylim(-0.25,2.5*np.pi)
#plt.gca().yaxis.set_major_locator(MaxNLocator(5))
plt.gca().set_yticks([0,np.pi,2.*np.pi])
plt.gca().set_yticklabels([r'$0$',r'$\pi$',r'$2\pi$'])
plt.legend(bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,borderaxespad=0.1)

fig=plt.gcf()
fig.savefig('breathers.pdf')
if showPreview:
    plt.show()
plt.clf()

plt.xlabel(r'$x$')
plt.ylabel(r'$\partial_{\phi\phi}V(\phi_{breather})$',labelpad=0.5)

plt.gca().set_yticks([-1,0,1])
xvals = np.linspace(-10., 10., 101)
for i in range(len(vvals)):
    yvals=eff_mass(xvals, 0., vvals[i])
    plt.plot(xvals, yvals, label=r'$v=$'+vlabels[i])

#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.ylim(-1.2,1.2)
plt.legend(bbox_to_anchor=(0.,0.,1.08,1.),loc='lower right',borderaxespad=0.25)

fig=plt.gcf()
fig.savefig('effmass_breathers.pdf')
if showPreview:
    plt.show()
plt.clf()
