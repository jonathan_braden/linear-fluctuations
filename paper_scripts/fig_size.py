#!/usr/bin/env python
import numpy as np

fig_sizes=np.array([0.23,0.32,0.45,0.48,0.6,0.75,0.95,1.])
text_size=430.2

fig_width=fig_sizes*text_size
golden_ratio=0.5*(5.**0.5-1.)
inches_per_pt = 1.0 / 72.27

print "430.2 pt linewidth (a4paper JHEP)"
for sz in fig_sizes:
    fig_width=text_size*sz
    fig_height=fig_width*golden_ratio
    print sz, "width (inches) = ",fig_width*inches_per_pt,"height (inches) = ",fig_height*inches_per_pt

text_size=442.29
fig_width=fig_sizes*text_size
print "\n442.29 pt linewidth (letterpaper JHEP)"
for sz in fig_sizes:
    fig_width=text_size*sz
    fig_height=fig_width*golden_ratio
    print sz, "width (inches) = ",fig_width*inches_per_pt,"height (inches) = ",fig_height*inches_per_pt
